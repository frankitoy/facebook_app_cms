<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version2 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->createTable('shortland_street_special_purchase_log', array(
             'id' => 
             array(
              'type' => 'integer',
              'length' => '8',
              'autoincrement' => '1',
              'primary' => '1',
             ),
             'facebook_id' => 
             array(
              'type' => 'string',
              'length' => '32',
             ),
             'order_id' => 
             array(
              'type' => 'string',
              'length' => '32',
             ),
             'status' => 
             array(
              'type' => 'enum',
              'values' => 
              array(
              0 => 'placed',
              1 => 'reserved',
              2 => 'settled',
              3 => 'canceled',
              ),
              'default' => 'placed',
              'length' => '',
             ),
             'is_test' => 
             array(
              'type' => 'boolean',
              'default' => '0',
              'length' => '25',
             ),
             'created_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             'updated_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             ), array(
             'indexes' => 
             array(
              'shortland_street_special_purchase_log_facebook_id' => 
              array(
              'fields' => 
              array(
               0 => 'facebook_id',
              ),
              ),
              'shortland_street_special_purchase_log_order_id' => 
              array(
              'fields' => 
              array(
               0 => 'order_id',
              ),
              ),
             ),
             'primary' => 
             array(
              0 => 'id',
             ),
             ));
    }

    public function down()
    {
        $this->dropTable('shortland_street_special_purchase_log');
    }
}