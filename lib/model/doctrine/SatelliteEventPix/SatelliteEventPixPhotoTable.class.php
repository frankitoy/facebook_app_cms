<?php

/**
 * SatelliteEventPixPhotoTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class SatelliteEventPixPhotoTable extends PluginSatelliteEventPixPhotoTable
{
    /**
     * Returns an instance of this class.
     *
     * @return object SatelliteEventPixPhotoTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('SatelliteEventPixPhoto');
    }
    
    public function getWebPath()
    {
    	return str_replace(sfConfig::get('sf_web_dir'), '', $this->getUploadPath());
    }
    
    public function getUploadPath()
    {
    	return sfConfig::get('sf_upload_dir') . '/app/satellite-event-pix';
    }
    
    public function getQueryForPhotoList($event)
    {
    	return $this->createQuery()
    	   ->where('is_public = ? and event = ?', array(1, $event))
    	   ->orderBy('created_at DESC');
    }
    
    public function findOneByViewingCodeAndEvent($code, $event)
    {
    	return $this->createQuery()
    	   ->where('is_public = ? AND viewing_code = ? AND event = ?', array(1, $code, $event))
    	   ->fetchOne();
    }
}