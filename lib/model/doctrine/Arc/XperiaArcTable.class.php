<?php

/**
 * XperiaArcTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class XperiaArcTable extends PluginXperiaArcTable
{
    /**
     * Returns an instance of this class.
     *
     * @return object XperiaArcTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('XperiaArc');
    }
    public function getEntriesForExport()
  {
     $export = $this->createQuery('entry')
      ->select('entry.first_name, entry.surname, entry.email, entry.answer, entry.bonus, entry.created_at as entry_date')
      ->orderBy('entry.created_at ASC')
      ->fetchArray();
     
     return $export;
  }
}