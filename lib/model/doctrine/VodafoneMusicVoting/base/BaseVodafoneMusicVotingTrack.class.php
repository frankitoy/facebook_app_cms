<?php

/**
 * BaseVodafoneMusicVotingTrack
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property string $artist
 * @property string $track
 * @property integer $track_id
 * @property boolean $has_preview
 * @property Doctrine_Collection $Votes
 * 
 * @package    Facebook
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseVodafoneMusicVotingTrack extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('vodafone_music_voting_track');
        $this->hasColumn('artist', 'string', 200, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 200,
             ));
        $this->hasColumn('track', 'string', 200, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 200,
             ));
        $this->hasColumn('track_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('has_preview', 'boolean', null, array(
             'type' => 'boolean',
             'default' => false,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('VodafoneMusicVotingVote as Votes', array(
             'local' => 'id',
             'foreign' => 'track_id'));
    }
}