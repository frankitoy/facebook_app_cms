<?php

/**
 * BaseVodafoneMusicVotingVote
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $track_id
 * @property integer $entry_id
 * @property VodafoneMusicVotingTrack $VodafoneMusicVotingTrack
 * @property VodafoneMusicVotingEntry $VodafoneMusicVotingEntry
 * 
 * @package    Facebook
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseVodafoneMusicVotingVote extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('vodafone_music_voting_vote');
        $this->hasColumn('track_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('entry_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('VodafoneMusicVotingTrack', array(
             'local' => 'track_id',
             'foreign' => 'id'));

        $this->hasOne('VodafoneMusicVotingEntry', array(
             'local' => 'entry_id',
             'foreign' => 'id'));
    }
}