<?php

/**
 * PluginVodafone50kTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class PluginVodafone50kTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object PluginVodafone50kTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PluginVodafone50k');
    }
}