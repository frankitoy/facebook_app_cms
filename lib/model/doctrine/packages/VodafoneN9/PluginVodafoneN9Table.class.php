<?php

/**
 * PluginVodafoneN9Table
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class PluginVodafoneN9Table extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object PluginVodafoneN9Table
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PluginVodafoneN9');
    }
}