<?php

/**
 * BaseSplashPlanetLeaderboard
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property int $score
 * @property int $facebook_id
 * 
 * @method string                  getFirstName()   Returns the current record's "first_name" value
 * @method string                  getLastName()    Returns the current record's "last_name" value
 * @method string                  getEmail()       Returns the current record's "email" value
 * @method int                     getScore()       Returns the current record's "score" value
 * @method int                     getFacebookId()  Returns the current record's "facebook_id" value
 * @method SplashPlanetLeaderboard setFirstName()   Sets the current record's "first_name" value
 * @method SplashPlanetLeaderboard setLastName()    Sets the current record's "last_name" value
 * @method SplashPlanetLeaderboard setEmail()       Sets the current record's "email" value
 * @method SplashPlanetLeaderboard setScore()       Sets the current record's "score" value
 * @method SplashPlanetLeaderboard setFacebookId()  Sets the current record's "facebook_id" value
 * 
 * @package    Facebook
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseSplashPlanetLeaderboard extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('splash_planet_leaderboard');
        $this->hasColumn('first_name', 'string', 255, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 255,
             ));
        $this->hasColumn('last_name', 'string', 255, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 255,
             ));
        $this->hasColumn('email', 'string', 255, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 255,
             ));
        $this->hasColumn('score', 'int', 30, array(
             'type' => 'int',
             'length' => 30,
             ));
        $this->hasColumn('facebook_id', 'int', 30, array(
             'type' => 'int',
             'length' => 30,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $timestampable0 = new Doctrine_Template_Timestampable(array(
             'created' => 
             array(
              'name' => 'created_at',
              'type' => 'timestamp',
             ),
             'updated' => 
             array(
              'disabled' => true,
             ),
             ));
        $this->actAs($timestampable0);
    }
}