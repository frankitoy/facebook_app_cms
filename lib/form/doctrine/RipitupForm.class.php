<?php

/**
 * Ripitup form.
 *
 * @package    Facebook
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class RipitupForm extends BaseRipitupForm
{
  public function configure()
  {
    unset($this['created_at'],$this['updated_at']);
    //$this->widgetSchema['answer'] = new sfWidgetFormSelect(array('choices' => array("1983","1979","1999")));
    $this->validatorSchema['email'] = new sfValidatorEmail(array(),array('invalid' => 'Enter a valid email address'));
    
  }
}
