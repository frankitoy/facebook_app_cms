<?php

/**
 * VodafoneN9 form.
 *
 * @package    Facebook
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class VodafoneN9Form extends BaseVodafoneN9Form
{
  public function configure()
  {
    unset($this['created_at'], $this['updated_at']);  
      
    $this->widgetSchema['accept_tc'] = new sfWidgetFormInputCheckbox();
    $this->widgetSchema['photo'] = new sfWidgetFormInputFile();
    
    $this->validatorSchema['accept_tc'] = new sfValidatorBoolean();
    $this->validatorSchema['email'] = new sfValidatorEmail();
    
    $dirs = sfConfig::get('app_vodafone_nokia_n9_upload');     
    $this->validatorSchema['photo'] = new sfValidatorFile(array(
      'mime_types'  => 'web_images',
      'max_size'    => 2 * 1024 * 1024,
      'path'        => sfConfig::get('sf_upload_dir').$dirs['folder'].$dirs['source']
    ));
    
    
  }
}
