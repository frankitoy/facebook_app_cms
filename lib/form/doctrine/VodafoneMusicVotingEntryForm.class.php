<?php

/**
 * VodafoneMusicVotingEntry form.
 *
 * @package    Facebook
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class VodafoneMusicVotingEntryForm extends BaseVodafoneMusicVotingEntryForm
{
  public function configure()
  {
    unset($this['created_at'], $this['updated_at'], $this['id']);  
      
    $this->widgetSchema['confirm_email'] = new sfWidgetFormInput();
    $this->widgetSchema['music_alerts']->setOption('expanded', true);
    $this->widgetSchema['music_alerts']->setOption('choices', array('email' => 'Email me', 'text' => 'Text me'));
    $this->widgetSchema['accept_tc'] = new sfWidgetFormInputCheckbox();
    
    $this->validatorSchema['email'] = new sfValidatorEmail();
    $this->validatorSchema['confirm_email'] = new sfValidatorEmail();
    $this->validatorSchema['accept_tc'] = new sfValidatorBoolean(array('required' => true));
    
    $this->validatorSchema->setPostValidator(new sfValidatorSchemaCompare('email',sfValidatorSchemaCompare::EQUAL,'confirm_email'));
    
    
  }
}
