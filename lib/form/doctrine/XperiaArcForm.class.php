<?php

/**
 * XperiaArc form.
 *
 * @package    Facebook
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class XperiaArcForm extends BaseXperiaArcForm
{
  public function configure()
  {
    // First Name
    $this->validatorSchema['first_name']->setOption('required', true);
    $this->validatorSchema['first_name']->setMessage('required', 'Please enter your first name');
    
    // Surname
    $this->validatorSchema['surname']->setOption('required', true);
    $this->validatorSchema['surname']->setMessage('required', 'Please enter your surname');
    
    // Email
    $this->validatorSchema['email'] = new sfValidatorEmail(array('required' => true));
    $this->validatorSchema['email']->setMessage('required', 'Please enter your email');
    $this->validatorSchema['email']->setMessage('invalid', 'Please enter a valid email');
    
    // Answer
    $this->validatorSchema['answer']->setOption('required', true);
    $this->validatorSchema['answer']->setMessage('required', 'Please enter your answer');
    
    // T&C
    $this->widgetSchema['terms'] = new sfWidgetFormInputCheckbox;
    $this->validatorSchema['terms'] = new sfValidatorBoolean(array('required' => true));
    $this->validatorSchema['terms']->setMessage('required', 'I am over 18 years of age and have agreed to the terms and conditions');
    
    $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
      new sfValidatorDoctrineUnique(array('model' => 'XperiaArc', 'column' => array('email')), array('invalid' => 'You have already entered this month'))
    )));
    
    unset(
      $this['created_at'],
      $this['updated_at']
    );
  }
}
