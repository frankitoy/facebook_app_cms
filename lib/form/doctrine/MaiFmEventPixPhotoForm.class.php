<?php

/**
 * MaiFmEventPixPhoto form.
 *
 * @package    Facebook
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class MaiFmEventPixPhotoForm extends BaseMaiFmEventPixPhotoForm
{
	public function configure()
	{
		$photo = $this->getObject();
		
		// Photo
		$this->validatorSchema['photo'] = new sfValidatorFile(array('required' => empty($photo->photo), 'mime_types' => 'web_images', 'path' => $this->getTable()->getUploadPath()));
		
		// Caption
		$this->widgetSchema['caption'] = new sfWidgetFormTextarea(array(), array('rows' => 2, 'cols' => 70));
		
		// Email
		$this->widgetSchema['email'] = new sfWidgetFormInputText;
		$this->validatorSchema['email'] = new sfValidatorEmail(array('required' => false));
		
		unset(
		    $this['viewing_code'],
            $this['created_at'],
            $this['updated_at']
		);
	}
	
	public function getTable()
	{
		return Doctrine::getTable($this->getModelName());
	}
}
