<?php

/**
 * Vodafone200 form.
 *
 * @package    Facebook
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class Vodafone200Form extends BaseVodafone200Form
{
  public function configure()
  {
      unset($this->validatorSchema['created_at']);
        unset($this->validatorSchema['updated_at']);

        unset($this->widgetSchema['created_at']);
        unset($this->widgetSchema['updated_at']);
  }
}
