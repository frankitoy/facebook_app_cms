<?php

/**
 * Share form.
 *
 * @package    Facebook
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ShareForm extends BaseShareForm
{
  public function configure()
  {
    $share = $this->getObject();
    
    // Title
    $this->validatorSchema['title']->setOption('required', true);

    // Type
    $this->widgetSchema['type'] = new sfWidgetFormSelect(array('choices' => $this->getChoicesForTypeList()));
    $this->validatorSchema['type']->setOption('required', true);

    // Url
    $this->widgetSchema['url']->setAttribute('size', 60);
    $this->validatorSchema['url'] = new sfValidatorUrl(array('required' => true));

    // Image
    $this->widgetSchema['image'] = new sfWidgetFormInputFileEditable(array('file_src' => empty($share->image) ? false : $share->getImageWebPath(), 'edit_mode' => true, 'is_image' => true, 'with_delete' => false));
    $this->validatorSchema['image'] = new sfValidatorFile(array('required' => empty($share->image), 'path' => $share->getTable()->getUploadDir(), 'mime_types' => 'web_images'));

    // Description
    $this->widgetSchema['description']->setAttribute('cols', 80);
    $this->validatorSchema['description']->setOption('required', true);

    unset(
      $this['created_at'],
      $this['updated_at']
    );
  }
  
  public function getChoicesForTypeList()
  {
    $file = sfConfig::get('sf_data_dir').'/facebook-open-graph-types.yml';
    
    $choices = array_merge(array('' => 'Please Select'), sfYamlConfigHandler::parseYaml($file));
    
    return $choices;
  }
}
