<?php

/**
 * Page form.
 *
 * @package    Facebook
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PageForm extends BasePageForm
{
  public function configure()
  {
    // Name
    $this->widgetSchema['name']->setAttribute('size', 50);
    $this->validatorSchema['name']->setOption('required', true);

    // Content
    $this->widgetSchema['content']->setAttribute('rows', 30);
    $this->widgetSchema['content']->setAttribute('cols', 100);
    $this->validatorSchema['content']->setOption('required', true);
    
    // Live from
    $this->widgetSchema['live_from']->setOption('date', array('format' => '%day%/%month%/%year%'));
    
    // Live until
    $this->widgetSchema['live_until']->setOption('date', array('format' => '%day%/%month%/%year%'));
    
    if ($this->getObject()->isNew())
    {
      unset($this['slug']);
    }
    else
    {
      $this->widgetSchema->moveField('slug', 'after', 'name');
    }
    
    unset(
      $this['app_id'],
      $this['created_at'],
      $this['updated_at']
    );
  }
}
