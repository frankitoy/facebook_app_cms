<?php

/**
 * Application form.
 *
 * @package    Facebook
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ApplicationForm extends BaseApplicationForm
{
  public function configure()
  {
    // App ID
    $this->widgetSchema['app_id']->setOption('label', 'App ID');
    
    // PHP SDK version
    $this->widgetSchema['php_sdk_version'] = new sfWidgetFormSelect(array('choices' => $this->getChoicesForPhpSdkVersion()));
    $this->validatorSchema['php_sdk_version'] = new sfValidatorChoice(array('choices' => array_keys($this->getChoicesForPhpSdkVersion())));
    
    // Jquery version
    $this->widgetSchema['jquery_version'] = new sfWidgetFormSelect(array('choices' => $this->getChoicesForJqueryVersion()));
    $this->validatorSchema['jquery_version'] = new sfValidatorChoice(array('choices' => array_keys($this->getChoicesForJqueryVersion())));
    
    // GA Id
    $this->widgetSchema['ga_id']->setOption('label', 'Google Analytics ID');
    
    // Assets
    $this->widgetSchema['assets'] = new sfWidgetFormInputFile;
    $this->validatorSchema['assets'] = new sfValidatorFile(array('required' => false));
    
    if ($this->getObject()->isNew())
    {
      unset($this['slug']);
    }
    else
    {
      $this->widgetSchema->moveField('slug', 'first');
    }
    
    unset(
      $this['name'],
      $this['description'],
      $this['link'],
      $this['like_gate_id'],
      $this['channel_url'],
      $this['created_at'],
      $this['updated_at']
    );
  }
  
  public function getChoicesForPhpSdkVersion()
  {
    $phpSdkVersions = sfConfig::get('app_facebook_php_sdk_versions', array());
    
    return array_combine($phpSdkVersions, $phpSdkVersions);
  }
  
  public function getChoicesForJqueryVersion()
  {
    $jqueryVersions = sfConfig::get('app_jquery_versions', array());
    
    return array_combine($jqueryVersions, $jqueryVersions);
  }
  
  public function save($con = null)
  {
    $application = parent::save($con);
    
    $application->saveAssets($this->getValue('assets'));
    
    return $application;
  }
  
  protected function updateAssetsColumn(sfValidatedFile $file = null)
  {
    return false;
  }
}
