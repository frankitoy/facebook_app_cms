<?php

/**
 * FuelPlus form.
 *
 * @package    Facebook
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class FuelPlusForm extends BaseFuelPlusForm
{
  private $wordLimit = 25;
  
  public function configure()
  {
    // First Name
    $this->validatorSchema['first_name']->setOption('required', true);
    $this->validatorSchema['first_name']->setMessage('required', 'Please enter your first name');
    
    // Surname
    $this->validatorSchema['surname']->setOption('required', true);
    $this->validatorSchema['surname']->setMessage('required', 'Please enter your surname');
    
    // Email
    $this->validatorSchema['email'] = new sfValidatorEmail(array('required' => true));
    $this->validatorSchema['email']->setMessage('required', 'Please enter your email');
    $this->validatorSchema['email']->setMessage('invalid', 'Please enter a valid email');
    
    // Post code
    $this->validatorSchema['postcode']->setOption('required', true);
    $this->validatorSchema['postcode']->setMessage('required', 'Please enter your postcode');
    
    // Birth date
    $years = array();
    for ($a=1940; $a<1994; $a++) { $years[$a] = $a; }
    $years = array_reverse($years);
    
    $months = array();
    for ($a=1; $a <=12; $a++) {$months[$a] = date("F", mktime(0,0,0,$a,1,1)); }
    
    $this->widgetSchema['birthdate'] = new sfWidgetFormDate;
    $this->widgetSchema['birthdate']->setOption('years', $years);
    $this->widgetSchema['birthdate']->setOption('months', $months);
    $this->validatorSchema['birthdate']->setOption('required', true);
    $this->validatorSchema['birthdate']->setMessage('required', 'Please enter your birth Date');
    
    // Answer
    $this->widgetSchema['answer'] = new sfWidgetFormTextarea;
    $this->validatorSchema['answer']->setOption('required', true);
    $this->validatorSchema['answer']->setMessage('required', 'Please enter your answer');
    $this->validatorSchema['answer']->setMessage('max_length', 'Please enter an answer less than 25 words');
    $this->widgetSchema->moveField('answer', 'before', 'first_name');
    
    // T&C
    $this->widgetSchema['terms'] = new sfWidgetFormInputCheckbox;
    $this->validatorSchema['terms'] = new sfValidatorBoolean(array('required' => true));
    $this->validatorSchema['terms']->setMessage('required', 'I am over 18 years of age and have agreed to the terms and conditions');
    
//    $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
//      new sfValidatorDoctrineUnique(array('model' => 'FuelPlus', 'column' => array('email')), array('invalid' => 'You have already entered this month')),
//      new sfValidatorCallback(array('callback' => array($this, 'validateAnswer')))
//    )));
    
    unset(
      $this['created_at'],
      $this['updated_at']
    );
  }
  
  public function validateAnswer($validator, $values)
  {
    if ($this->wordLimit < str_word_count($values['answer']))
    {
      throw new sfValidatorErrorSchema($validator, array('answer' => new sfValidatorError($validator, 'Please enter an answer less than 25 words')));
    }
    
    return $values;
  }
}
