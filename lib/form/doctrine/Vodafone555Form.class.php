<?php

/**
 * Vodafone555 form.
 *
 * @package    Facebook
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class Vodafone555Form extends BaseVodafone555Form
{
  public function configure()
  {
    unset($this['created_at'],$this['updated_at']);
    $this->validatorSchema['email'] = new sfValidatorEmail(array(),array('invalid'=>'Please enter a valid email'));
  }
}
