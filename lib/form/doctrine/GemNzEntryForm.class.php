<?php

/**
 * GemNzEntry form.
 *
 * @package    Facebook
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class GemNzEntryForm extends BaseGemNzEntryForm
{
  private $wordLimit = 25;
  
  public function configure()
  {
    // Answer
    $this->validatorSchema['answer']->setOption('required', true);
    $this->validatorSchema['answer']->setMessage('required', 'Please enter your answer');
    $this->validatorSchema['answer']->setMessage('max_length', 'Please enter an answer less than 25 words');
    $this->widgetSchema->moveField('answer', 'before', 'first_name');
    
    // First Name
    $this->validatorSchema['first_name']->setOption('required', true);
    $this->validatorSchema['first_name']->setMessage('required', 'Please enter your first name');
    
    // Surname
    $this->validatorSchema['surname']->setOption('required', true);
    $this->validatorSchema['surname']->setMessage('required', 'Please enter your last name');
    
    // Email
    $this->validatorSchema['email'] = new sfValidatorEmail(array('required' => true));
    $this->validatorSchema['email']->setMessage('required', 'Please enter your email');
    $this->validatorSchema['email']->setMessage('invalid', 'Please enter a valid email');
    
    // Mobile phone
    $this->validatorSchema['mobile_phone'] = new sfValidatorRegex(array('pattern' => '#^[0-9\(\)\s\+-]+$#', 'required' => true), array('required' => 'Please enter your mobile phone', 'invalid' => 'Please enter a valid phone number'));
    
    // Town
    $this->validatorSchema['town']->setOption('required', true);
    $this->validatorSchema['town']->setMessage('required', 'Please enter your town');
    
    // Dob
    $this->widgetSchema['dob'] = new TextFieldDateWidget(array('format' => '%day%<span>/</span>%month%<span>/</span>%year%'));
    $this->validatorSchema['dob']->setOption('required', true);
    $this->validatorSchema['dob']->setMessage('required', 'Please enter your date of birth');
    $this->validatorSchema['dob']->setMessage('invalid', 'Please enter your date of birth in the following format: DD / MM / YYYY');
        
    // T&C
    $this->widgetSchema['terms'] = new sfWidgetFormInputCheckbox;
    $this->validatorSchema['terms'] = new sfValidatorBoolean(array('required' => true));
    $this->validatorSchema['terms']->setMessage('required', 'You must read and understand the Terms and Conditions and be over the age of 18');
    
    $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
      new sfValidatorDoctrineUnique(array('model' => 'GemNzEntry', 'column' => array('email')), array('invalid' => 'You have already entered this month')),
      new sfValidatorCallback(array('callback' => array($this, 'validateAnswer'))),
      new sfValidatorCallback(array('callback' => array($this, 'validateIs18OrOver')))
    )));
    
    unset(
      $this['created_at'],
      $this['updated_at']
    );
  }
  
  public function validateAnswer($validator, $values)
  {
    if ($this->wordLimit < str_word_count($values['answer']))
    {
      throw new sfValidatorErrorSchema($validator, array('answer' => new sfValidatorError($validator, 'Please enter an answer less than 25 words')));
    }
    
    return $values;
  }
  
  public function validateIs18OrOver($validator, $values)
  {
    list($year, $month, $day) = explode('-', $values['dob']);
    
    $dobTs = mktime(0, 0, 0, $month, $day, $year);
    
    $isOver18 = ((time() - $dobTs) > 568024668);
    
    if (!$isOver18)
    {
      throw new sfValidatorErrorSchema($validator, array('dob' => new sfValidatorError($validator, 'Sorry but you must be 18 or older to enter this competition')));
    }
    
    return $values;
  }
}
