<?php

/**
 * Vodafone200 form base class.
 *
 * @method Vodafone200 getObject() Returns the current form's model object
 *
 * @package    Facebook
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseVodafone200Form extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'name'             => new sfWidgetFormInputText(),
      'email'            => new sfWidgetFormInputText(),
      'phone'            => new sfWidgetFormInputText(),
      'answer'           => new sfWidgetFormInputText(),
      'add_mailing_list' => new sfWidgetFormInputCheckbox(),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'             => new sfValidatorString(array('max_length' => 64, 'required' => false)),
      'email'            => new sfValidatorString(array('max_length' => 64, 'required' => false)),
      'phone'            => new sfValidatorString(array('max_length' => 64, 'required' => false)),
      'answer'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'add_mailing_list' => new sfValidatorBoolean(array('required' => false)),
      'created_at'       => new sfValidatorDateTime(),
      'updated_at'       => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('vodafone200[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Vodafone200';
  }

}
