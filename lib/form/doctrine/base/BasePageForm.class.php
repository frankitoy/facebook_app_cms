<?php

/**
 * Page form base class.
 *
 * @method Page getObject() Returns the current form's model object
 *
 * @package    Facebook
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePageForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'title'            => new sfWidgetFormInputText(),
      'name'             => new sfWidgetFormInputText(),
      'content'          => new sfWidgetFormTextarea(),
      'live_from'        => new sfWidgetFormDateTime(),
      'live_until'       => new sfWidgetFormDateTime(),
      'app_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Application'), 'add_empty' => true)),
      'is_like_page'     => new sfWidgetFormInputCheckbox(),
      'is_tab_page'      => new sfWidgetFormInputCheckbox(),
      'is_canvas_page'   => new sfWidgetFormInputCheckbox(),
      'is_bookmark_page' => new sfWidgetFormInputCheckbox(),
      'is_published'     => new sfWidgetFormInputCheckbox(),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
      'slug'             => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'title'            => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'name'             => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'content'          => new sfValidatorString(array('required' => false)),
      'live_from'        => new sfValidatorDateTime(array('required' => false)),
      'live_until'       => new sfValidatorDateTime(array('required' => false)),
      'app_id'           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Application'), 'required' => false)),
      'is_like_page'     => new sfValidatorBoolean(array('required' => false)),
      'is_tab_page'      => new sfValidatorBoolean(array('required' => false)),
      'is_canvas_page'   => new sfValidatorBoolean(array('required' => false)),
      'is_bookmark_page' => new sfValidatorBoolean(array('required' => false)),
      'is_published'     => new sfValidatorBoolean(array('required' => false)),
      'created_at'       => new sfValidatorDateTime(),
      'updated_at'       => new sfValidatorDateTime(),
      'slug'             => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'Page', 'column' => array('slug', 'app_id', 'name')))
    );

    $this->widgetSchema->setNameFormat('page[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Page';
  }

}
