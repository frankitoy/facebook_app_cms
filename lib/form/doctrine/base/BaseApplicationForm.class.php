<?php

/**
 * Application form base class.
 *
 * @method Application getObject() Returns the current form's model object
 *
 * @package    Facebook
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseApplicationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'name'                 => new sfWidgetFormInputText(),
      'description'          => new sfWidgetFormTextarea(),
      'link'                 => new sfWidgetFormInputText(),
      'app_id'               => new sfWidgetFormInputText(),
      'app_secret'           => new sfWidgetFormInputText(),
      'like_gate_id'         => new sfWidgetFormInputText(),
      'php_sdk_version'      => new sfWidgetFormInputText(),
      'jquery_version'       => new sfWidgetFormInputText(),
      'use_js_sdk'           => new sfWidgetFormInputCheckbox(),
      'use_debug_javascript' => new sfWidgetFormInputCheckbox(),
      'load_asynchronous'    => new sfWidgetFormInputCheckbox(),
      'enable_cookie'        => new sfWidgetFormInputCheckbox(),
      'enable_logging'       => new sfWidgetFormInputCheckbox(),
      'fetch_status'         => new sfWidgetFormInputCheckbox(),
      'parse_xfbml'          => new sfWidgetFormInputCheckbox(),
      'channel_url'          => new sfWidgetFormInputText(),
      'ga_id'                => new sfWidgetFormInputText(),
      'created_at'           => new sfWidgetFormDateTime(),
      'updated_at'           => new sfWidgetFormDateTime(),
      'slug'                 => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'                 => new sfValidatorString(array('max_length' => 64, 'required' => false)),
      'description'          => new sfValidatorString(array('required' => false)),
      'link'                 => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'app_id'               => new sfValidatorString(array('max_length' => 32, 'required' => false)),
      'app_secret'           => new sfValidatorString(array('max_length' => 32, 'required' => false)),
      'like_gate_id'         => new sfValidatorString(array('max_length' => 32, 'required' => false)),
      'php_sdk_version'      => new sfValidatorString(array('max_length' => 16, 'required' => false)),
      'jquery_version'       => new sfValidatorString(array('max_length' => 8, 'required' => false)),
      'use_js_sdk'           => new sfValidatorBoolean(array('required' => false)),
      'use_debug_javascript' => new sfValidatorBoolean(array('required' => false)),
      'load_asynchronous'    => new sfValidatorBoolean(array('required' => false)),
      'enable_cookie'        => new sfValidatorBoolean(array('required' => false)),
      'enable_logging'       => new sfValidatorBoolean(array('required' => false)),
      'fetch_status'         => new sfValidatorBoolean(array('required' => false)),
      'parse_xfbml'          => new sfValidatorBoolean(array('required' => false)),
      'channel_url'          => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'ga_id'                => new sfValidatorString(array('max_length' => 16, 'required' => false)),
      'created_at'           => new sfValidatorDateTime(),
      'updated_at'           => new sfValidatorDateTime(),
      'slug'                 => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorAnd(array(
        new sfValidatorDoctrineUnique(array('model' => 'Application', 'column' => array('app_id'))),
        new sfValidatorDoctrineUnique(array('model' => 'Application', 'column' => array('slug'))),
      ))
    );

    $this->widgetSchema->setNameFormat('application[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Application';
  }

}
