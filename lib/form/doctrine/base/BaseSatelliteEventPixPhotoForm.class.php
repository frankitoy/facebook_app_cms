<?php

/**
 * SatelliteEventPixPhoto form base class.
 *
 * @method SatelliteEventPixPhoto getObject() Returns the current form's model object
 *
 * @package    Facebook
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseSatelliteEventPixPhotoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'photo'        => new sfWidgetFormInputText(),
      'caption'      => new sfWidgetFormInputText(),
      'event'        => new sfWidgetFormInputText(),
      'viewing_code' => new sfWidgetFormInputText(),
      'is_public'    => new sfWidgetFormInputCheckbox(),
      'created_at'   => new sfWidgetFormDateTime(),
      'updated_at'   => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'photo'        => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'caption'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'event'        => new sfValidatorString(array('max_length' => 64, 'required' => false)),
      'viewing_code' => new sfValidatorString(array('max_length' => 8, 'required' => false)),
      'is_public'    => new sfValidatorBoolean(array('required' => false)),
      'created_at'   => new sfValidatorDateTime(),
      'updated_at'   => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('satellite_event_pix_photo[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SatelliteEventPixPhoto';
  }

}
