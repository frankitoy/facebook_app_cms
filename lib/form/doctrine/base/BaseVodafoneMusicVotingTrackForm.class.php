<?php

/**
 * VodafoneMusicVotingTrack form base class.
 *
 * @method VodafoneMusicVotingTrack getObject() Returns the current form's model object
 *
 * @package    Facebook
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseVodafoneMusicVotingTrackForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'artist'      => new sfWidgetFormInputText(),
      'track'       => new sfWidgetFormInputText(),
      'track_id'    => new sfWidgetFormInputText(),
      'has_preview' => new sfWidgetFormInputCheckbox(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'artist'      => new sfValidatorString(array('max_length' => 200)),
      'track'       => new sfValidatorString(array('max_length' => 200)),
      'track_id'    => new sfValidatorInteger(),
      'has_preview' => new sfValidatorBoolean(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('vodafone_music_voting_track[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VodafoneMusicVotingTrack';
  }

}
