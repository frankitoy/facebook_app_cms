<?php

/**
 * VodafoneMusicVotingEntry form base class.
 *
 * @method VodafoneMusicVotingEntry getObject() Returns the current form's model object
 *
 * @package    Facebook
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseVodafoneMusicVotingEntryForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'name'              => new sfWidgetFormInputText(),
      'email'             => new sfWidgetFormInputText(),
      'mobile'            => new sfWidgetFormInputText(),
      'music_alerts'      => new sfWidgetFormChoice(array('choices' => array('email' => 'email', 'text' => 'text'))),
      'music_alert_email' => new sfWidgetFormInputCheckbox(),
      'music_alert_text'  => new sfWidgetFormInputCheckbox(),
      'notify'            => new sfWidgetFormInputCheckbox(),
      'created_at'        => new sfWidgetFormDateTime(),
      'updated_at'        => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'              => new sfValidatorString(array('max_length' => 200)),
      'email'             => new sfValidatorString(array('max_length' => 200)),
      'mobile'            => new sfValidatorString(array('max_length' => 200)),
      'music_alerts'      => new sfValidatorChoice(array('choices' => array(0 => 'email', 1 => 'text'), 'required' => false)),
      'music_alert_email' => new sfValidatorBoolean(array('required' => false)),
      'music_alert_text'  => new sfValidatorBoolean(array('required' => false)),
      'notify'            => new sfValidatorBoolean(array('required' => false)),
      'created_at'        => new sfValidatorDateTime(),
      'updated_at'        => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('vodafone_music_voting_entry[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VodafoneMusicVotingEntry';
  }

}
