<?php

/**
 * ShortlandStreetSpecialPurchaseLog form base class.
 *
 * @method ShortlandStreetSpecialPurchaseLog getObject() Returns the current form's model object
 *
 * @package    Facebook
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseShortlandStreetSpecialPurchaseLogForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'facebook_id'  => new sfWidgetFormInputText(),
      'order_id'     => new sfWidgetFormInputText(),
      'status'       => new sfWidgetFormChoice(array('choices' => array('placed' => 'placed', 'reserved' => 'reserved', 'settled' => 'settled', 'canceled' => 'canceled'))),
      'is_test'      => new sfWidgetFormInputCheckbox(),
      'access_token' => new sfWidgetFormInputText(),
      'created_at'   => new sfWidgetFormDateTime(),
      'updated_at'   => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'facebook_id'  => new sfValidatorString(array('max_length' => 32, 'required' => false)),
      'order_id'     => new sfValidatorString(array('max_length' => 32, 'required' => false)),
      'status'       => new sfValidatorChoice(array('choices' => array(0 => 'placed', 1 => 'reserved', 2 => 'settled', 3 => 'canceled'), 'required' => false)),
      'is_test'      => new sfValidatorBoolean(array('required' => false)),
      'access_token' => new sfValidatorString(array('max_length' => 32, 'required' => false)),
      'created_at'   => new sfValidatorDateTime(),
      'updated_at'   => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('shortland_street_special_purchase_log[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ShortlandStreetSpecialPurchaseLog';
  }

}
