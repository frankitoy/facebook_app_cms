<?php

/**
 * SplashPlanetLeaderboard form base class.
 *
 * @method SplashPlanetLeaderboard getObject() Returns the current form's model object
 *
 * @package    Facebook
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseSplashPlanetLeaderboardForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'first_name'  => new sfWidgetFormInputText(),
      'last_name'   => new sfWidgetFormInputText(),
      'email'       => new sfWidgetFormInputText(),
      'score'       => new sfWidgetFormInputText(),
      'facebook_id' => new sfWidgetFormInputText(),
      'created_at'  => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'first_name'  => new sfValidatorString(array('max_length' => 255)),
      'last_name'   => new sfValidatorString(array('max_length' => 255)),
      'email'       => new sfValidatorString(array('max_length' => 255)),
      'score'       => new sfValidatorPass(array('required' => false)),
      'facebook_id' => new sfValidatorPass(array('required' => false)),
      'created_at'  => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('splash_planet_leaderboard[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SplashPlanetLeaderboard';
  }

}
