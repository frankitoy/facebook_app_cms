<?php

/**
 * GemEntry form base class.
 *
 * @method GemEntry getObject() Returns the current form's model object
 *
 * @package    Facebook
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseGemEntryForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'first_name'   => new sfWidgetFormInputText(),
      'surname'      => new sfWidgetFormInputText(),
      'email'        => new sfWidgetFormInputText(),
      'mobile_phone' => new sfWidgetFormInputText(),
      'state'        => new sfWidgetFormInputText(),
      'answer'       => new sfWidgetFormInputText(),
      'created_at'   => new sfWidgetFormDateTime(),
      'updated_at'   => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'first_name'   => new sfValidatorString(array('max_length' => 64, 'required' => false)),
      'surname'      => new sfValidatorString(array('max_length' => 64, 'required' => false)),
      'email'        => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'mobile_phone' => new sfValidatorString(array('max_length' => 16, 'required' => false)),
      'state'        => new sfValidatorString(array('max_length' => 32, 'required' => false)),
      'answer'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'created_at'   => new sfValidatorDateTime(),
      'updated_at'   => new sfValidatorDateTime(),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'GemEntry', 'column' => array('email')))
    );

    $this->widgetSchema->setNameFormat('gem_entry[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'GemEntry';
  }

}
