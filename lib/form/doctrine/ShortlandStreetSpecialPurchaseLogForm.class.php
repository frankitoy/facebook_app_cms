<?php

/**
 * ShortlandStreetSpecialPurchaseLog form.
 *
 * @package    Facebook
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ShortlandStreetSpecialPurchaseLogForm extends BaseShortlandStreetSpecialPurchaseLogForm
{
  public function configure()
  {
    $this->validatorSchema['facebook_id']->setOption('required', true);
    $this->validatorSchema['order_id']->setOption('required', true);
    $this->validatorSchema['status']->setOption('required', true);
    
    unset(
      $this['access_token'],
      $this['created_at'],
      $this['updated_at']
    );
  }
}
