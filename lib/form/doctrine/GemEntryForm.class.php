<?php

/**
 * GemEntry form.
 *
 * @package    Facebook
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class GemEntryForm extends BaseGemEntryForm
{
  private $wordLimit = 25;
  
  public function configure()
  {
    // Answer
    $this->widgetSchema['answer'] = new sfWidgetFormTextarea;
    $this->validatorSchema['answer']->setOption('required', true);
    $this->validatorSchema['answer']->setMessage('required', 'Please enter your answer');
    $this->validatorSchema['answer']->setMessage('max_length', 'Please enter an answer less than 25 words');
    $this->widgetSchema->moveField('answer', 'before', 'first_name');
    
    // First Name
    $this->validatorSchema['first_name']->setOption('required', true);
    $this->validatorSchema['first_name']->setMessage('required', 'Please enter your first name');
    
    // Surname
    $this->validatorSchema['surname']->setOption('required', true);
    $this->validatorSchema['surname']->setMessage('required', 'Please enter your surname');
    
    // Email
    $this->validatorSchema['email'] = new sfValidatorEmail(array('required' => true));
    $this->validatorSchema['email']->setMessage('required', 'Please enter your email');
    $this->validatorSchema['email']->setMessage('invalid', 'Please enter a valid email');
    
    // Mobile phone
    $this->validatorSchema['mobile_phone']->setOption('required', true);
    $this->validatorSchema['mobile_phone']->setMessage('required', 'Please enter your mobile phone');
    
    // State
    $this->validatorSchema['state']->setOption('required', true);
    $this->validatorSchema['state']->setMessage('required', 'Please enter your state');
    
    // T&C
    $this->widgetSchema['terms'] = new sfWidgetFormInputCheckbox;
    $this->validatorSchema['terms'] = new sfValidatorBoolean(array('required' => true));
    $this->validatorSchema['terms']->setMessage('required', 'You must read and understand the Terms and Conditions and be over the age of 18');
    
    $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
      new sfValidatorDoctrineUnique(array('model' => 'GemEntry', 'column' => array('email')), array('invalid' => 'You have already entered this month')),
      new sfValidatorCallback(array('callback' => array($this, 'validateAnswer')))
    )));
    
    unset(
      $this['created_at'],
      $this['updated_at']
    );
  }
  
  public function validateAnswer($validator, $values)
  {
    if ($this->wordLimit < str_word_count($values['answer']))
    {
      throw new sfValidatorErrorSchema($validator, array('answer' => new sfValidatorError($validator, 'Please enter an answer less than 25 words')));
    }
    
    return $values;
  }
}
