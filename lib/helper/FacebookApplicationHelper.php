<?php

function application_url($slug, $absolute = false)
{
  $application = sfContext::getInstance()->getRequest()->getAttribute('application');
  
  if ($slug instanceof sfOutputEscaper)
  {
    $slug = $slug->getRawValue();
  }
  
  if ($slug instanceof Page)
  {
    $slug = $slug->slug;
  }
  
  return url_for(array('sf_route' => 'page', 'app_slug' => $application->slug, 'page_slug' => $slug), $absolute);
}

function application_landing_url($type, $absolute = false)
{
  $application = sfContext::getInstance()->getRequest()->getAttribute('application');
  
  return url_for(array('sf_route' => 'application_landing_page', 'app_slug' => $application->slug, 'type' => $type), $absolute);
}

function use_application_stylesheet($css, $position = '', $options = array())
{
  $application = sfContext::getInstance()->getRequest()->getAttribute('application');
  
  $css = $application->getAssetWebPath($css);
  
  use_stylesheet($css, $position, $options);
}

function use_application_javascript($js, $position = '', $options = array())
{
  $application = sfContext::getInstance()->getRequest()->getAttribute('application');
  
  $js = $application->getAssetWebPath($js);
  
  use_javascript($js, $position, $options);
}

function application_asset_path($source, $absolute = false)
{
  $application = sfContext::getInstance()->getRequest()->getAttribute('application');
  
  $dir = trim($application->getAssetWebPath(), '/');
  $ext = substr($source, strrpos($source, '.') + 1);
  
  return _compute_public_path($source, $dir, $ext, $absolute);
}