<?php

/**
 * Application filter form.
 *
 * @package    Facebook
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ApplicationFormFilter extends BaseApplicationFormFilter
{
  public function configure()
  {
    $this->widgetSchema->moveField('slug', 'after', 'name');
    
    unset(
      $this['link'],
      $this['app_secret'],
      $this['like_gate_id'],
      $this['php_sdk_version'],
      $this['jquery_version'],
      $this['use_js_sdk'],
      $this['use_debug_javascript'],
      $this['load_asynchronous'],
      $this['enable_cookie'],
      $this['enable_logging'],
      $this['fetch_status'],
      $this['parse_xfbml'],
      $this['channel_url'],
      $this['created_at'],
      $this['updated_at']
    );
  }
}
