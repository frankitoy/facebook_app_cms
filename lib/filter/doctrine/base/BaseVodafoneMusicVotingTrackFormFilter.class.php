<?php

/**
 * VodafoneMusicVotingTrack filter form base class.
 *
 * @package    Facebook
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseVodafoneMusicVotingTrackFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'artist'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'track'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'track_id'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'has_preview' => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
    ));

    $this->setValidators(array(
      'artist'      => new sfValidatorPass(array('required' => false)),
      'track'       => new sfValidatorPass(array('required' => false)),
      'track_id'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'has_preview' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
    ));

    $this->widgetSchema->setNameFormat('vodafone_music_voting_track_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VodafoneMusicVotingTrack';
  }

  public function getFields()
  {
    return array(
      'id'          => 'Number',
      'artist'      => 'Text',
      'track'       => 'Text',
      'track_id'    => 'Number',
      'has_preview' => 'Boolean',
    );
  }
}
