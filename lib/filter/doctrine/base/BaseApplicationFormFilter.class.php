<?php

/**
 * Application filter form base class.
 *
 * @package    Facebook
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseApplicationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'                 => new sfWidgetFormFilterInput(),
      'description'          => new sfWidgetFormFilterInput(),
      'link'                 => new sfWidgetFormFilterInput(),
      'app_id'               => new sfWidgetFormFilterInput(),
      'app_secret'           => new sfWidgetFormFilterInput(),
      'like_gate_id'         => new sfWidgetFormFilterInput(),
      'php_sdk_version'      => new sfWidgetFormFilterInput(),
      'jquery_version'       => new sfWidgetFormFilterInput(),
      'use_js_sdk'           => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'use_debug_javascript' => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'load_asynchronous'    => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'enable_cookie'        => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'enable_logging'       => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'fetch_status'         => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'parse_xfbml'          => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'channel_url'          => new sfWidgetFormFilterInput(),
      'ga_id'                => new sfWidgetFormFilterInput(),
      'created_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'slug'                 => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'name'                 => new sfValidatorPass(array('required' => false)),
      'description'          => new sfValidatorPass(array('required' => false)),
      'link'                 => new sfValidatorPass(array('required' => false)),
      'app_id'               => new sfValidatorPass(array('required' => false)),
      'app_secret'           => new sfValidatorPass(array('required' => false)),
      'like_gate_id'         => new sfValidatorPass(array('required' => false)),
      'php_sdk_version'      => new sfValidatorPass(array('required' => false)),
      'jquery_version'       => new sfValidatorPass(array('required' => false)),
      'use_js_sdk'           => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'use_debug_javascript' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'load_asynchronous'    => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'enable_cookie'        => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'enable_logging'       => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'fetch_status'         => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'parse_xfbml'          => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'channel_url'          => new sfValidatorPass(array('required' => false)),
      'ga_id'                => new sfValidatorPass(array('required' => false)),
      'created_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'slug'                 => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('application_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Application';
  }

  public function getFields()
  {
    return array(
      'id'                   => 'Number',
      'name'                 => 'Text',
      'description'          => 'Text',
      'link'                 => 'Text',
      'app_id'               => 'Text',
      'app_secret'           => 'Text',
      'like_gate_id'         => 'Text',
      'php_sdk_version'      => 'Text',
      'jquery_version'       => 'Text',
      'use_js_sdk'           => 'Boolean',
      'use_debug_javascript' => 'Boolean',
      'load_asynchronous'    => 'Boolean',
      'enable_cookie'        => 'Boolean',
      'enable_logging'       => 'Boolean',
      'fetch_status'         => 'Boolean',
      'parse_xfbml'          => 'Boolean',
      'channel_url'          => 'Text',
      'ga_id'                => 'Text',
      'created_at'           => 'Date',
      'updated_at'           => 'Date',
      'slug'                 => 'Text',
    );
  }
}
