<?php

/**
 * VodafoneMusicVotingVote filter form base class.
 *
 * @package    Facebook
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseVodafoneMusicVotingVoteFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'track_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VodafoneMusicVotingTrack'), 'add_empty' => true)),
      'entry_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VodafoneMusicVotingEntry'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'track_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('VodafoneMusicVotingTrack'), 'column' => 'id')),
      'entry_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('VodafoneMusicVotingEntry'), 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('vodafone_music_voting_vote_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VodafoneMusicVotingVote';
  }

  public function getFields()
  {
    return array(
      'id'       => 'Number',
      'track_id' => 'ForeignKey',
      'entry_id' => 'ForeignKey',
    );
  }
}
