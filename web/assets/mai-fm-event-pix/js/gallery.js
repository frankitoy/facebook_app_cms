var viewModel = {};

(function(){

	$(function(){
		$('#gallery .view').hover(function(){
			$('.navigation', this).fadeIn('fast');
		}, function(){
			$('.navigation', this).fadeOut('fast');
		});
		
		$('#searchForm').bind('submit', function(){
			viewModel.search();
			return false;
		}).bind('search', function(){
			viewModel.search();
		});
	});
	
	viewModel = {
		selectedIndex:  ko.observable(-1),
		page:			ko.observable(-1),
		url:			null,
		searchUrl:		null,
		photos:			ko.observableArray(),
		searchMessageTimeout: null
	};
	
	viewModel.selectedPhoto = ko.dependentObservable({
		'read': function()
		{
			var selectedIndex = this.selectedIndex();
			
			if (0 > selectedIndex) return false;

			var photos = this.photos();
			
			if (photos[selectedIndex])
			{
				return photos[selectedIndex];
			}
			else
			{
				return false;
			}
		},
		'write': function(index)
		{
			this.selectedIndex(index);
			
			$('#gallery .thumbs a').removeClass('selected');
			$('#gallery .thumbs li').eq(index).find('a').addClass('selected');
		},
		'owner': viewModel
	});
	
	viewModel.currentPage = ko.dependentObservable({
		'read': function()
		{
			return this.page();
		},
		'write': function(newPage)
		{
			if (newPage < 1) return;
			
			if (!this.url) return;
			
			$.getJSON(this.url.replace('_PAGE', newPage), function(data)
			{
		    	if (0 < data.length)
	    		{
		    		viewModel.photos([]);
		    		
		    		var photos = viewModel.photos();
		    	    for (var x in data) photos.push(data[x]);
		    	    
		    	    viewModel.photos(photos);
		    	    viewModel.page(newPage);
		    	    viewModel.selectedPhoto(0);
	    		}
		    });
		},
		'owner': viewModel
	});
	
	viewModel.nextPage = function()
	{
		this.currentPage(this.page() + 1);
	}
	
	viewModel.prevPage = function()
	{
		this.currentPage(this.page() - 1);
	}
	
	viewModel.nextImage = function()
	{
		var newIndex = this.selectedIndex() + 1;
			
		if ((newIndex + 1) > $('#gallery .thumbs li').length)
		{
			this.nextPage();
		}
		else
		{
			this.selectedPhoto(newIndex);
		}
	}
	
	viewModel.prevImage = function()
	{
		var newIndex = this.selectedIndex() - 1;
			
		if (0 > newIndex)
		{
			this.prevPage();
		}
		else
		{
			this.selectedPhoto(newIndex);
		}
	}
	
	viewModel.search = function()
	{
		var code = $('#code').val();
		
		if ('' == code)
		{
			this.currentPage(1);
			return;
		}
		
		$.getJSON(this.searchUrl.replace('_CODE', code), function(data)
		{
	    	if (0 < data.length)
    		{
	    		viewModel.photos([]);
	    		
	    		var photos = viewModel.photos();
	    	    for (var x in data) photos.push(data[x]);
	    	    
	    	    viewModel.photos(photos);
	    	    viewModel.page(1);
	    	    viewModel.selectedPhoto(0);
    		}
	    	else
    		{
	    		$('#gallery .searchMessage').html('That photo could not be found.');

	    		clearTimeout(viewModel.searchMessageTimeout);
	    		
	    		viewModel.searchMessageTimeout = setTimeout(function(){
	    			$('#gallery .searchMessage').fadeOut('fast', function(){
	    				$(this).html('').show();
	    			});
	    		}, 3000);
    		}
	    });
	}
	
})(jQuery);