(function($){

  $(function(){
    $('.facebook_share').facebookshare();
    
    $('a.facebook_popup').live('click', function()
    {
      var $link = $(this);
      
      $.get($link.attr('href'), function(response){
        var title;
        
        if ($link.attr('title'))
        {
          title = $link.attr('title');
        }
        else
        {
          title = $link.html();
        }
        
        fbalert(title, response);
      });
      
      return false;
    });
  });
  
  $('body').bind('fb_init', function(){
    FB.Canvas.setAutoResize();
  });

  $.fn.facebookshare = function()
  {
    $(this).click(function()
    {
      var data = $(this).data();
      
      FB.ui({
        method: 'feed',
        name: data.shareName,
        link: data.shareLink,
        caption: data.shareCaption,
        picture: data.sharePicture,
        description: data.shareDescription
      });

      return false;
    });
  };
  
})(jQuery);


function fbalert(title, message)
{
  var $markup = $('<div class="pop_container_advanced_big" id="pop_container_advanced_big" style="display:none;">\
                      <div id="pop_content" class="pop_content">\
                          <h2 class="dialog_title">\
                              <span></span>\
                          </h2>\
                          <div class="dialog_content">\
                              <div id="error_container" class="content_overflow">\
                              </div>\
                              <br /><br />\
                              <input type="button" class="fb_buttons right" id="closeme_big" value="Okay" />\
                              <br class="clear" /><br />\
                          </div>\
                      </div>\
                  </div>');

  $('.dialog_title span', $markup).html(title);
  $('#error_container', $markup).html(message);
  
  $('.fb_buttons', $markup).click(function(){
    $markup.fadeOut('fast', function(){
      $markup.remove();
    });
  });
  
  $('body').append($markup);
  
  $markup.fadeIn('fast');
}


function fbnofity(title, message)
{
  var $markup = $('<div class="pop_container_advanced_small" id="pop_container_advanced_big" style="display:none;">\
                      <div id="pop_content" class="pop_content">\
                          <h2 class="dialog_title">\
                              <span></span>\
                          </h2>\
                          <div class="dialog_content">\
                              <div id="error_container" style="overflow: auto; height: 25px;">\
                              </div>\
                              <br /><br />\
                              <input type="button" class="fb_buttons right" id="closeme_big" value="Okay" />\
                              <br class="clear" /><br />\
                          </div>\
                      </div>\
                  </div>');

  $('.dialog_title span', $markup).html(title);
  $('#error_container', $markup).html(message);
  
  $('.fb_buttons', $markup).click(function(){
    $markup.fadeOut('fast', function(){
      $markup.remove();
    });
  });
  
  $('body').append($markup);
  
  $markup.fadeIn('fast');
}


/*
** notify alternative design
*/

function fbalert_alt(title, message)
{
  var $markup = $('<div class="pop_container_alt" id="pop_container_advanced_big" style="display:none;">\
                      <div id="pop_content" class="pop_content_alt">\
                          <div class=".dialog_content_alt">\
                              <div id="error_container" class="content_overflow">\
                              </div>\
                                <img class="fb_buttons_alt" id="closeme_big" src="https://facebook.satelliteapps.co.nz/uploads/app/arc/closebtn.png" style="position: absolute; top: -10px; right: -10px; cursor: pointer" />\
                          </div>\
                      </div>\
                  </div>');

  //$('.dialog_title span', $markup).html(title);
  $('#error_container', $markup).html(message);
  
  $('.fb_buttons_alt', $markup).click(function(){
    $markup.fadeOut('fast', function(){
      $markup.remove();
    });
  });
  
  $('body').append($markup);
  
  $markup.fadeIn('fast');
}

function fbnofity_alt(title, message)
{
  var $markup = $('<div class="pop_container_notify_alt" id="pop_container_advanced_big" style="display:none;">\
                      <div id="pop_content" class=".pop_content_alt">\
                          <div class=".dialog_content_alt">\
                              <link href="http://fonts.googleapis.com/css?family=Kreon" rel="stylesheet" type="text/css">\
                                <br />\
                                <span style="font-family: Kreon, serif; font-size: 21px; color: #2A388E;">\
                                Error\
                                </span><br /><br />\
                              <div id="error_container" style="overflow: auto; height: 25px;">\
                              </div>\
                                <img class="fb_buttons_alt" id="closeme_big" src="https://facebook.satelliteapps.co.nz/uploads/app/arc/closebtn.png" style="position: absolute; top: -10px; right: -10px; cursor: pointer" />\
                          </div>\
                      </div>\
                  </div>');

  //$('.dialog_title span', $markup).html(title);
  $('#error_container', $markup).html(message);
  
  $('.fb_buttons_alt', $markup).click(function(){
    $markup.fadeOut('fast', function(){
      $markup.remove();
    });
  });
  
  $('body').append($markup);
  
  $markup.fadeIn('fast');
}