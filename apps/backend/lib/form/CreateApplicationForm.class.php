<?php

class CreateApplicationForm extends ApplicationForm
{
  public function configure()
  {
    parent::configure();
    
    unset(
      $this['php_sdk_version']
    );
  }
}