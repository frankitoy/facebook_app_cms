<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>
  </head>
  <body>
  
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
        
          <?php if ($sf_user->isAuthenticated()): ?>
            <div class="btn-group pull-right">
              <a class="btn dropdown-toggle btn-inverse" data-toggle="dropdown" href="#"><i class="icon-user icon-white"></i> <?php echo $sf_user->getName() ?> <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo url_for('@simple_auth_logout') ?>"><i class="icon-off"></i> Logout</a></li>
              </ul>
            </div>
          <?php endif; ?>
        
          <a href="" class="brand">Satellite Media Facebook CMS</a>
          
        </div>
      </div>
    </div>
    
    <div class="container">
      <?php echo $sf_content ?>
    </div>
    
  </body>
</html>
