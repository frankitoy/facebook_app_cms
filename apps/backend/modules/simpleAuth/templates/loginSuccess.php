<div class="page-header">
  <h1>Login</h1>
</div>

<form action="<?php echo url_for('simple_auth_login') ?>" method="post" class="form form-horizontal">

  <?php echo $form ?>
  
  <div class="form-actions">
    <button class="btn btn-primary" type="submit">Login</button>
  </div>
  
</form>