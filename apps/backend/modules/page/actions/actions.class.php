<?php

require_once dirname(__FILE__).'/../lib/pageGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/pageGeneratorHelper.class.php';

/**
 * page actions.
 *
 * @package    Facebook
 * @subpackage page
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class pageActions extends autoPageActions
{
  public function preExecute()
  {
    parent::preExecute();

    $applicationId = $this->getUser()->getAttribute('application_id', null, 'application');

    if (!$applicationId) $this->redirect('@application');

    $this->dispatcher->connect('admin.build_query', array($this, 'filterHeirarchicalResults'));
  }
  
  public function filterHeirarchicalResults(sfEvent $event, Doctrine_Query $query)
  {
    $rootAlias = $query->getRootAlias();

    return $query->addWhere(sprintf('%s.app_id = ?', $rootAlias), $this->getUser()->getAttribute('application_id', null, 'application'));
  }
}
