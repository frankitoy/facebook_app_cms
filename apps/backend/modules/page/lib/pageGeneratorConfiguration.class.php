<?php

/**
 * page module configuration.
 *
 * @package    Facebook
 * @subpackage page
 * @author     Your name here
 * @version    SVN: $Id: configuration.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class pageGeneratorConfiguration extends BasePageGeneratorConfiguration
{
  private function getApplicationId()
  {
    return sfContext::getInstance()->getUser()->getAttribute('application_id', null, 'application');
  }

  public function getForm($object = null, $options = array())
  {
    if (null === $object) $object = new Page;

    if ($object->isNew()) $object->app_id = $this->getApplicationId();

    return parent::getForm($object, $options);
  }
}
