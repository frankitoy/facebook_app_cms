<?php

require_once dirname(__FILE__).'/../lib/vodafone_n9GeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/vodafone_n9GeneratorHelper.class.php';

/**
 * vodafone_n9 actions.
 *
 * @package    Facebook
 * @subpackage vodafone_n9
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class vodafone_n9Actions extends autoVodafone_n9Actions
{
  public function executeBatchApprove(sfWebRequest $request)
  {
    $ids = $request->getParameter('ids');
 
    $q = Doctrine_Query::create()
      ->from('VodafoneN9 j')
      ->whereIn('j.id', $ids);
 
    foreach ($q->execute() as $entry)
    {
      $entry->approve(true);
    }
 
    $this->getUser()->setFlash('notice', 'The selected entries have been approved.');
 
    $this->redirect('vodafone_n9');
  }
}
