<?php

require_once dirname(__FILE__).'/../lib/shareGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/shareGeneratorHelper.class.php';

/**
 * share actions.
 *
 * @package    Facebook
 * @subpackage share
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class shareActions extends autoShareActions
{
}
