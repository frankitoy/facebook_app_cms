<html xmlns="http://www.w3.org/1999/xhtml"
  xmlns:fb="https://www.facebook.com/2008/fbml">
  <head>
    <title>My Add to Page Dialog Page</title>
  </head>
  <body>
  
	<form action="#" onsubmit="addToPage(document.getElementById('url').value); return false;">
		<h1><?php echo $app->getName() ?> (<?php echo $app->getAppId() ?>)</h1>
		<p>1. Enter the URL that should be loaded inside the Facebook Iframe below:</p>
		<p><label for="url">URL:</label>
			<input id="url" type="text" size="100" value="<?php echo public_path($app->getSlug(), true) ?>" /></p>
		<p>2. Click the following link:</p>
	  <p id="manualLink"></p>
		<p>Not working? Try clicking here instead:</p>
		<p><button type="submit">Continue</button></p>
	</form>
	
    <div id='fb-root'></div>
    <script>
      window.fbAsyncInit = function() {
        FB.init({"appId":"<?php echo $app->getAppId() ?>","cookie":true,"logging":false,"status":true,"xfbml":false,"oauth":true,"channelURL":"http:\/\/facebook.satelliteapps.co.nz\/channel.html"});
      };

      (function() {
        var e = document.createElement('script'); e.async = true;
        e.src = 'http://connect.facebook.net/en_US/all.js';
        document.getElementById('fb-root').appendChild(e);
      }());

      function addToPage(url) {
        FB.ui({
          method: 'pagetab',
          redirect_uri: url,
        });
      }
      
      var updateManualLink = function(){
        var link = 'https://www.facebook.com/dialog/pagetab?app_id=<?php echo $app->getAppId() ?>&next=' + encodeURIComponent(document.getElementById('url').value);
        document.getElementById('manualLink').innerHTML = '<a href="'+link+'" target="_blank">'+link+'</a>';
      };
      
      document.getElementById('url').onkeyup = function(){
        updateManualLink();
      };
      
      updateManualLink();
    
    </script>
  </body>
</html>
