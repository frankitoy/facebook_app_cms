<?php

require_once dirname(__FILE__).'/../lib/applicationGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/applicationGeneratorHelper.class.php';

/**
 * application actions.
 *
 * @package    Facebook
 * @subpackage application
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class applicationActions extends autoApplicationActions
{
  public function executeAddToPage(sfWebRequest $request)
  {
	$this->setLayout(false);
	
	$this->app = $this->getRoute()->getObject();
  }

  public function executeBrowsePages(sfWebRequest $request)
  {
    $this->getUser()->setAttribute('application_id', $this->getRoute()->getObject()->id, 'application');

    $this->redirect('@page');
  }
}
