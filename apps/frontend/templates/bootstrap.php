<!DOCTYPE html>
<html xmlns:fb="http://www.facebook.com/2008/fbml">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <?php include_stylesheets() ?>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
  </head>
  <body>
  
    <?php include_slot('navbar') ?>
    
    <div class="container">
      <?php echo $sf_content ?>
    </div>
    
    <?php include_javascripts() ?>
    
  </body>
</html>
