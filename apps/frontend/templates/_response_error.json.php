<?php

$response = array('status' => 'ERROR');

if (isset($payload))
{
  $response['payload'] = $payload->getRawValue();
}

include_partial('global/response', array('response' => $response));
