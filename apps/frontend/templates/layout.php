<?php $application = $sf_request->getAttribute('application'); ?><!DOCTYPE html>
<html xmlns:fb="http://www.facebook.com/2008/fbml">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <?php /* <link rel="shortcut icon" href="/favicon.ico" /> */ ?>
    <?php include_stylesheets() ?>
    <!--[if IE]>
      <style type="text/css">
        .pop_container_advanced, .pop_container_advanced_big, .pop_container_advanced_small {
           background:transparent;
           filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99525252,endColorstr=#99525252);
           zoom: 1;
         }
      </style>
    <![endif]-->
  </head>
  <body>
    
    <?php echo $sf_content ?>
    
    <script type="text/javascript" src="<?php printf('http%s://ajax.googleapis.com/ajax/libs/jquery/%s/jquery.min.js', empty($_SERVER['HTTPS']) ? '' : 's', $application->jquery_version) ?>"></script>
    
    <?php include_javascripts() ?>
    
    <?php include_partial('application/embed_js_sdk', array('application' => $application)) ?>
    
    <?php include_slot('javascript') ?>
    
    <?php include_partial('application/embed_tracking', array('application' => $application)) ?>
    
  </body>
</html>
