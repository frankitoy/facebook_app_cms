<form action="<?php echo application_url($page) ?>" method="post" onsubmit="return checkfields()">

    Full Name <span class="red">*</span>  
    <div class="form_row first_name_form_row">
    <?php echo $form['first_name'] ?>
    </div>

    <div class="form_row surname_form_row">
    <?php echo $form['surname'] ?>
    </div>

    <br class="clear" />

    <div class="label_names">
        <div class="small_label fname fl">first name</div>
        <div class="small_label lname fr">last name</div>
    </div>

    <br class="clear" />

    Email <span class="red">*</span>  

    <div class="form_row email_form_row">
    <?php echo $form['email'] ?>
    </div>

    <br class="clear" />

    Postcode <span class="red">*</span> 

    <div class="form_row postcode_form_row">
    <?php echo $form['postcode'] ?>
    </div>

    <br class="clear" />

    Birth Date <span class="red">*</span> 

    <div class="form_row birthdate_form_row">
    <?php echo $form['birthdate'] ?>
    </div>  

    <div class="label_names">
        <div class="small_label fname fl">Month / Day / Year</div>
    </div>

    <br class="clear" />

    Answer <span class="red">*</span> 

    <div class="form_row answer_form_row">
    <?php echo $form['answer'] ?>
    </div>

    <div class="form_row terms_form_row">
    <?php echo $form['terms'] ?>
            I am over 18 years of age and have agreed to the <a href="http://www.powerade.com.au/Competition-Terms.html" class="terms_label" target="_blank">terms and conditions</a>
    </div>

    <div class="submit_container">
        <input type="submit" value="Submit" />
    </div>
  
  <?php echo $form->renderHiddenFields() ?>
  
</form>

<script>
    
    function checkfields() {
        
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var title = "Powerade Fuel+";
        
        word_count = 0;
        var matches = $("#fuel_plus_answer").val().match(/\b/g);
        if(matches) {
            word_count = matches.length/2;
        }
        
        if (!$('#fuel_plus_first_name').val()) {
            fbnofity(title, "Please write your first name");
            return false;
        } else if (!$('#fuel_plus_surname').val()) {
            fbnofity(title, "Please write your last name");
            return false;
        } else if (!$('#fuel_plus_email').val()) {
            fbnofity(title, "Please write your email address");
            return false;
        } else if (!emailReg.test($('#fuel_plus_email').val())) {
            fbnofity(title, "Please check your email address");
            return false;
        } else if (!$('#fuel_plus_postcode').val()) {
            fbnofity(title, "Please write your postcode");
            return false;
        } else if (!$('#fuel_plus_birthdate_month').val()) {
            fbnofity(title, "Please complete your birth date");
            return false;
        } else if (!$('#fuel_plus_birthdate_day').val()) {
            fbnofity(title, "Please complete your birth date");
            return false;
        } else if (!$('#fuel_plus_birthdate_year').val()) {
            fbnofity(title, "Please complete your birth date");
            return false;
        } else if (!$("#fuel_plus_answer").val().length) {
            fbnofity(title, "Please write your answer");
            return false;
        } else if (word_count > 25) {
            fbnofity(title, "Your answer must not exceed 25 words");
            return false;
        } else if (!$("#fuel_plus_terms").is(':checked')) {
            fbnofity(title, "You must agree to Powerade Fuel+ terms and conditions");
            return false;
        } else {
            return true;
        }
        
    }
    
</script>