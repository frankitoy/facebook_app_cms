<?php

$entries = $entries->getRawValue();

$filename = tempnam(sys_get_temp_dir(), '');

$fp = fopen($filename, 'w');

fputcsv($fp, array(
  'First Name',
  'Surname',
  'Email',
  'Birth Date',
  'Postcode',
  'Answer',
  'Entry Date'
));

foreach ($entries as $entry)
{
  fputcsv($fp, array(
    $entry['first_name'],
    $entry['surname'],
    $entry['email'],
    $entry['birthdate'],
    $entry['postcode'],
    $entry['answer'],
    $entry['entry_date']
  ));
}

fclose($fp);

echo file_get_contents($filename);

unlink ($filename);
