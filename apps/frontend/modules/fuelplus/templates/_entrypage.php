<img class="absolute" style="top: 0; left: 0" src="<?php echo application_asset_path('form-background.jpg') ?>" />

<div class=" absolute main_description">
    POWERADE FUEL+ is a new energy drink for sports from<br/>POWERADE. With a combination of caffeine energy and<br />electrolytes, POWERADE FUEL+ provides you an energy kick<br />so that you can START OFF, SWITCHED ON.
</div> 

<div class="absolute youtube_video">
    <div id="show_youtube">
        <object width="494" height="344"> 
<!--            <param name="movie" value="http://www.youtube.com/v/A0Whl3Y2foI?version=3&amp;hl=en_US&amp;autoplay=1"></param>-->
            <param name="movie" value="http://www.youtube.com/v/A0Whl3Y2foI?version=3&amp;hl=en_US&amp"></param>
            <param name="allowFullScreen" value="true"></param>
            <param name="allowscriptaccess" value="always"></param>

            <embed src="http://www.youtube.com/v/A0Whl3Y2foI?version=3&amp;hl=en_US&amp;autoplay=1" type="application/x-shockwave-flash" width="494" height="344" allowscriptaccess="always" allowfullscreen="true"></embed>
        </object>
    </div>
</div>

<div class="absolute playstation_caption">
    In 25 words or less, tell us the best example of STARTING OFF, SWITCHED ON you've ever seen in sport.<br /><br />
    Every day for 14 days, we'll be giving the 2 best entries of the day a POWERADE FUEL+ sample pack. At the end of 2 weeks, the top 6 of these winners will also win a PS3 and PlayStation®Move pack each. The best stories may also be featured on the POWERADE Australia Facebook page. 
</div>

<div class="absolute entryfields">
    <?php include_component('fuelplus', 'entryForm') ?>
</div>

<div class="absolute footnotes">
    Competition closes 21st August at 17.00 EST. WINNERS WILL BE NOTIFIED VIA EMAIL BY 21ST AUGUST 2011. Prize Details: 2 best entries per day each win 1 case of POWERADE FUEL+ Berry Ice and 1 case POWERADE FUEL+ Mountain Blast. At the end of 2 weeks, top 6 best entries will also win a Sony Playstation 3 and Move pack. Competition only open to residents of Australia who are at least 18 years of age. Terms and Condition apply. Visit www.powerade.com.au for full competition details.
</div>

<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script>
    
    $("#show_video").click(function() {
        $(this).css('display', 'none');
        $("#show_youtube").css('display','inline');
    })
    
</script>-->