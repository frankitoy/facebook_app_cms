<?php if ($application->use_js_sdk) include_component('facebook', 'embedJsSdk', array(
  'asynchronous' => $application->load_asynchronous,
  'debug' => $application->use_debug_javascript,
  'appId' => $application->app_id,
  'cookie' => $application->enable_cookie,
  'logging' => $application->enable_logging,
  'session' => (version_compare('3', $application->php_sdk_version, '>=') ? $sf_user->getFacebookSession() : null),
  'status' => $application->fetch_status,
  'xfbml' => $application->parse_xfbml
));