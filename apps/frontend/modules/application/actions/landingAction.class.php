<?php

class landingAction extends sfAction
{
  public function preExecute()
  {
    $this->application = Doctrine::getTable('Application')->findOneBySlug($this->getRequestParameter('app_slug'));
    
    $this->forward404Unless($this->application && $this->application->exists());
    
    $this->getUser()->setApplication($this->application);
  }
  
  public function execute($request)
  {
    switch ($request->getParameter('type'))
    {
      case 'tab':
        $page = $this->application->TabPage;
        break;
    
      case 'canvas':
        $page = $this->application->CanvasPage;
        break;
    
      case 'bookmark':
        $page = $this->application->BookmarkPage;
        break;
      
      case 'credits':
        return $this->executeCreditsCallback($request);
    }
    
    $this->forward404Unless($page && $page->exists());
    
    $request->setParameter('page_slug', $page->slug);

    $this->forward('page', 'view');
  }
  
  private function executeCreditsCallback(sfWebRequest $request)
  {
    $callbackClass = sfConfig::get(sprintf('app_%s_credits_callback_action_class', $this->application->app_id));
    
    if ($callbackClass)
    {
      $sr = $this->getUser()->getFacebook()->getSignedRequest();
      
      // Validate that this is a real FB request, not just browsing to the URL
      $this->forward404Unless($sr);
      
      switch($request->getParameter('method'))
      {
        case 'payments_get_items':
          $this->forward($callbackClass, 'paymentsGetItems');
          
        case 'payments_status_update':
          $this->forward($callbackClass, 'paymentsStatusUpdate');
      }
    }
    
    $this->forward404('Unknown credits callback');
  }
}