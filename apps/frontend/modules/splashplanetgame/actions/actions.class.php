<?php
 
/**
 * splashplanetgame actions.
 *
 * @package    Facebook
 * @subpackage splashplanetgame
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class splashplanetgameActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $this->forward('splashplanetgame', $request->getParameter('page'));
  }
  
  public function executeLeaderboard($request)
  {
    
    $this->result = Doctrine::getTable('SplashPlanetLeaderboard')
                      ->createQuery()
                      ->orderBy('score DESC')
                      ->execute(array(),  Doctrine::HYDRATE_ARRAY); 
  }
  
  public function executeAddUser(sfWebRequest $request)
  {
    
//    $this->forward404Unless($request->isMethod('post'));
            
    $form = new SplashPlanetLeaderboardForm();
    $form->bind($request->getParameter($form->getName()));
    $this->setTemplate('form');

    if( $form->isValid()) {
       
      $form->save();
      return sfView::SUCCESS;
      
    } else {
       
      $this->form = $form;
      return sfView::ERROR;
      
    }

  }
  
  /*
   * by jay, 23.09.11
   * usage: /splash-planet-game/api/savescore/:facebookid/:email/:firstname/:lastname/:score
   * 
   */
  public function executeSaveScore(sfWebRequest $request) {
    
    // check if required parameters are present
    if (!$request->getParameter("score") || !$request->getParameter("facebookid")) {
        echo "0";
        die();
    }
      
    //check if hash is correct, returns 0 if it's a hack attempt
    list($score_raw, $hash_ball) = explode("-", $request->getParameter("score"));
    if ($hash_ball == sha1("splashplanetrocks")) {
        $score = $score_raw;
    } else {
        echo "0";
        die();
    }
    
    //$custom_query = Doctrine_Query::create()->select("s.*")->from("SplashPlanetLeaderboard s")->where("s.facebook_id = ?", $request->getParameter("facebookid"))->addWhere("s.score < ?", $request->getParameter("score"));
    $custom_query = Doctrine_Query::create()->select("s.*")->from("SplashPlanetLeaderboard s")->where("s.facebook_id = ?", $request->getParameter("facebookid"))->addWhere("s.score < ?", $score);
    $has_record = $custom_query->fetchArray();
    
    if(sizeof($has_record)) {
        // record existing, so just upgrade
        
        $update_query = Doctrine_Query::create()->update("SplashPlanetLeaderboard s")->set("s.score", $score)->where("facebook_id = ?", $request->getParameter("facebookid"))->execute();
        echo "2";
        die();
    } else {
        
        $custom_query = Doctrine_Query::create()->select("*")->from("SplashPlanetLeaderboard")->where("facebook_id = ?", $request->getParameter("facebookid"));
        $has_record = $custom_query->fetchArray();
        
        if (!sizeof($has_record)) {
            // new record
            $form = new SplashPlanetLeaderboard();

            $form->setFacebookId($request->getParameter("facebookid"));
            $form->setEmail($request->getParameter("email"));
            $form->setFirstName($request->getParameter("firstname"));
            $form->setLastName($request->getParameter("lastname"));
            $form->setScore($score);
            $form->setCreatedAt(date("Y-m-d g:i:s"));

            $form->save();
            echo "1";
        } else {
            echo "0";
        }
        
    }
    
    // no need for template, so kill it
    die();
  }
  
  public function executeDeleteRecord(sfWebRequest $request) {
      Doctrine_Query::create()
      ->delete()
      ->from('SplashPlanetLeaderboard')
      ->where('facebook_id = ?', $request->getParameter("facebookid"))
      ->execute();
      
      die();
  }
  
  /*
   * by jay, 23.09.11
   * usage: /splash-planet-game/download/entries
   */
  public function executeDownloadEntries(sfWebRequest $request) {
    
    $username = "splash";
    $password = "splash!";
    
    header('WWW-Authenticate: Basic realm="Arc Entries"');
    
    if (empty($_SERVER['PHP_AUTH_USER']) || empty($_SERVER['PHP_AUTH_PW']) ||
       ($username != $_SERVER['PHP_AUTH_USER'] || $password != $_SERVER['PHP_AUTH_PW']))
    {
      header('HTTP/1.0 401 Unauthorized');
      echo 'Login required';
      exit;
    } else {
        $this->getResponse()->setContentType('text/csv');
        $this->getResponse()->setHttpHeader('Content-disposition', 'attachment;filename=Entries.csv');
        $data = Doctrine_Query::create()->select("*")->from("SplashPlanetLeaderboard");
        $this->entries = $data->fetchArray();
    }
  }
}
