<?php

$entries = $entries->getRawValue();
$filename = tempnam(sys_get_temp_dir(), '');
$fp = fopen($filename, 'w');
fputcsv($fp, array(
  'Initials',
  'Last Name',
  'Email',
  'Score',
  'Facebook ID',
  'Entry Date'
));
foreach ($entries as $entry)
{
  fputcsv($fp, array(
    $entry['first_name'],
    $entry['last_name'],
    $entry['email'],
    $entry['score'],
    $entry['facebook_id'],
    $entry['created_at']
  ));
}

fclose($fp);
echo file_get_contents($filename);
unlink ($filename);
