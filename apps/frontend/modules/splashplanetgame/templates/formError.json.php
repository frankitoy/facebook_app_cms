<?php

$json = array('status' => 'ERROR', 'messages' => array());

if ($form->getGlobalErrors())
{
  foreach($form->getGlobalErrors() as $error)
  {
    $json['messages'][] = $error->__toString();
  }
}

foreach ($form as $fieldName => $field)
{
  if ($field->hasError())
  {
    $json['messages'][] = $field->getError()->__toString();
  }
}

echo json_encode($json);