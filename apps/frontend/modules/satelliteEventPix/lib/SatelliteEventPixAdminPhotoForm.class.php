<?php

class SatelliteEventPixAdminPhotoForm extends SatelliteEventPixPhotoForm
{
	public function configure()
	{
		parent::configure();
		
		unset(
          $this['photo'],
          $this['location'],
          $this['event'],
          $this['email']
		);
	}
}
