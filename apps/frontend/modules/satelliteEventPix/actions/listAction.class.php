<?php

class listAction extends sfAction
{
  public function execute($request)
  {
	$pager = new sfDoctrinePager('SatelliteEventPixPhoto', 28); // 4 rows of 7
	$pager->setPage($request->getParameter('page'));
	$pager->setQuery(Doctrine::getTable('SatelliteEventPixPhoto')->getQueryForPhotoList($request->getParameter('event')));
	$pager->init();
	  
	$this->pager = $pager;
  }
}