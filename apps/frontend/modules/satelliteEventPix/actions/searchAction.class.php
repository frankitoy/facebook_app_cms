<?php

class searchAction extends sfAction
{
  public function execute($request)
  {
    $code = strtoupper(trim($request->getParameter('code')));
    $event = $request->getParameter('event');
    
    $photo = Doctrine::getTable('SatelliteEventPixPhoto')->findOneByViewingCodeAndEvent($code, $event);
    $photos = array();
    
    if ($photo && $photo->exists())
    {
      $photos[] = $photo;
    }
    
    $this->photos = $photos;
  }
}