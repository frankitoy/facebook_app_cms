<?php

require_once dirname(__FILE__).'/../lib/satelliteEventPixGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/satelliteEventPixGeneratorHelper.class.php';

/**
 * satelliteEventPix actions.
 *
 * @package    Facebook
 * @subpackage satelliteEventPix
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class satelliteEventPixActions extends autoSatelliteEventPixActions
{
    public function requireAuthentication()
    {
      if (empty($_SERVER['PHP_AUTH_USER']) || empty($_SERVER['PHP_AUTH_PW']) || !($_SERVER['PHP_AUTH_USER'] == 'satellitepixadmin' && $_SERVER['PHP_AUTH_PW'] == 'x3haHacH'))
      {
        header('WWW-Authenticate: Basic realm="Satellite Event Pix"');
        header('HTTP/1.0 401 Unauthorized');
        echo 'This is a restricted area.';
        exit;
      }
    }
    
    public function preExecute()
    {
        $this->requireAuthentication();
        
        parent::preExecute();
        
        $this->setLayout('bootstrap');
    }
    
    public function executePublish(sfWebRequest $request)
    {
        $photo = $this->getRoute()->getObject();
        
        $photo->publish();
        
        $this->getUser()->setFlash('notice', 'Successfully published photo.');
        
        return $this->redirect('satellite_event_pix_photo_backend');
    }
    
    public function executeUnpublish(sfWebRequest $request)
    {
        $photo = $this->getRoute()->getObject();
        
        $photo->unpublish();
        
        $this->getUser()->setFlash('notice', 'Successfully unpublished photo.');
        
        return $this->redirect('satellite_event_pix_photo_backend');
    }
    
    public function executeBatchPublish(sfWebRequest $request)
    {
    	$table = Doctrine::getTable('SatelliteEventPixPhoto');
    	
        $photos = $table->createQuery()->whereIn('id', $request->getParameter('ids'))->execute();
        
        $conn = $table->getConnection();
        
        $conn->beginTransaction();
        
        try
        {
	        foreach ($photos as $photo)
	        {
	            $photo->publish();
	        }
	        
	        $conn->commit();
        }
        catch (Exception $ex)
        {
        	$conn->rollback();
        	
        	throw $ex;
        }
        
        $this->getUser()->setFlash('notice', 'Successfully published photos.');
    }

    public function executeBatchUnpublish(sfWebRequest $request)
    {
    
        $table = Doctrine::getTable('SatelliteEventPixPhoto');
        
        $photos = $table->createQuery()->whereIn('id', $request->getParameter('ids'))->execute();
        
        $conn = $table->getConnection();
        
        $conn->beginTransaction();
        
        try
        {
            foreach ($photos as $photo)
            {
                $photo->unpublish();
            }
            
            $conn->commit();
        }
        catch (Exception $ex)
        {
            $conn->rollback();
            
            throw $ex;
        }
        
        $this->getUser()->setFlash('notice', 'Successfully unpublished photos.');
    }
}
