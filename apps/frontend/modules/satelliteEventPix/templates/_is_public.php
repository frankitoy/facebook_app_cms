<?php if ($satellite_event_pix_photo->is_public): ?>
    <?php echo link_to(image_tag('/sfDoctrinePlugin/images/tick.png', array('alt' => 'Published')), 'satellite_event_pix_photo_backend_unpublish', array('id' => $satellite_event_pix_photo->id), array('post' => true)) ?>
<?php else: ?>
    <?php echo link_to(image_tag('/sfDoctrinePlugin/images/delete.png', array('alt' => 'Unpublished')), 'satellite_event_pix_photo_backend_publish', array('id' => $satellite_event_pix_photo->id), array('post' => true)) ?>
<?php endif; ?>