<?php use_javascript('/assets/satellite-event-pix/js/jquery.tmpl.min.js') ?>
<?php use_javascript('/assets/satellite-event-pix/js/knockout-1.2.1.js') ?>
<?php use_javascript('/assets/satellite-event-pix/js/gallery.js') ?>
<?php use_stylesheet('/assets/satellite-event-pix/css/main.css') ?>

<?php /*
<img src="<?php echo image_path('/assets/satellite-event-pix/images/header.jpg') ?>" alt="Check out the Satellite photos here." width="520" height="200"/>
*/ ?>

<div id="gallery" data-bind="template: 'galleryTemplate'"></div>

<script type="text/x-jquery-tmpl" id="galleryTemplate">
    <form id="searchForm"><label for="code">Search:</label><input type="search" id="code"/><button type="submit">Go</button><span class="searchMessage"></span></form>
    <div class="view" data-bind="template: { name: 'viewTemplate' }"></div>
    <div class="caption seethrough" data-bind="visible: selectedPhoto().caption, text: selectedPhoto().caption"></div>
    <div class="controls">
        <a href="#" class="next" data-bind="click: nextPage">Page &gt;</a>
        <a href="#" class="prev" data-bind="click: prevPage">&lt; Page</a>
    </div>
    <ul class="thumbs" data-bind="template: 'photoTemplate'"></ul>
</script>

<script type="text/x-jquery-tmpl" id="viewTemplate">
    <img data-bind="attr: { src: selectedPhoto().photo, alt: selectedPhoto().caption }" width="520" height="390"/>
    <div class="navigation">
        <a href="#" class="next seethrough" data-bind="click: nextImage">Next</a>
        <a href="#" class="prev seethrough" data-bind="click: prevImage">Previous</a>
    </div>
    
    
    <!-- share link added -->
    <div style="position: absolute; right: 7px; bottom: 12px; z-index: 99">
        <a target="_blank" href="https://www.facebook.com/sharer.php?u=http://facebook.satelliteapps.co.nz/share.php?i=${ selectedPhoto().photo }">
            <img src="/share.png" />
        </a>
    </div>
</script>

<script type="text/x-jquery-tmpl" id="photoTemplate">
{{each photos}}
    <li style="position: relative">
        <a href="${ photo }" data-bind="click: function(event){viewModel.selectedPhoto($(event.currentTarget).closest('li').index());}">
            <img src="${ thumbnail }" alt="${ caption }"/>
        </a>
    </li>
{{/each}}
</script>

<?php slot('javascript') ?>

<script type="text/javascript">

ko.applyBindings(viewModel);
viewModel.url = '<?php echo url_for('satellite_event_pix_photos_data', array('page' => '_PAGE', 'sf_format' => 'js', 'event' => $event)) ?>';
viewModel.searchUrl = '<?php echo url_for('satellite_event_pix_search', array('code' => '_CODE', 'sf_format' => 'js', 'event' => $event)) ?>';
viewModel.currentPage(<?php echo $sf_request->getParameter('page', 1) ?>);

</script>

<?php end_slot(); ?>