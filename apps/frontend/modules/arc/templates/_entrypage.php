<link href='http://fonts.googleapis.com/css?family=Kreon' rel='stylesheet' type='text/css'>

<img src="<?php echo application_asset_path('top_banner.jpg') ?>" />

<div class="competition_description">
    Discover the new Sony Ericsson Xperia&trade; arc Android smartphone and capture the True Colours of New Zealand the way they should be seen – with deeper, more true to life colours, even in dark settings. Capture perfect low light images using the Exmor&trade; R mobile sensor, then view them on screen with crystal clear definition from the Mobile BRAVIA&reg; engine. It's mobile photography with the visual brilliance of Sony.<br /><br />
    Sony Ericsson is giving away a stunning Xperia&trade; arc (RRP $999) every day for two weeks. <br /><br />
    Correctly identify one of the New Zealand landmarks shown below and you'll be in the draw to win one of the most entertaining Android smartphones yet!
</div>

<div class="main_screen">
    <div class="arc_phone">
        
        <?php
            // let's define the question
            $question_form = rand(1,6);
        ?>
        <img class="image_question" src="<?php echo application_asset_path('questions/0'. $question_form .'.jpg') ?>" />
    </div>
    
    <div class="android_popcorn"></div>
    
    <div class="greenball_overlay"></div>
    
    <div class="xperia_label"></div>
</div>

<br class="clearfix" />

<div class="entry_form">
    <?php include_component('arc', 'entryForm', array('question_form' => $question_form)) ?>
</div>

<br class="clearfix" />

<div class="footer_technologies"></div>

<?php include_partial('arc/footer'); ?>