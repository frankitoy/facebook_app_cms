<link href='http://fonts.googleapis.com/css?family=Kreon' rel='stylesheet' type='text/css'>

<script type="text/javascript" src="<?php echo application_asset_path('swfobject.js') ?>"></script>

<script type="text/javascript">
           var flashvars = {};
           var params = {};
           params.wmode = "transparent"; 
           var attributes = {};
           swfobject.embedSWF("<?php echo application_asset_path('Components_VidPlayer.swf') ?>", "flashDIV", "354", "201", "10.0.0", "<?php echo application_asset_path('playerProductInstall.swf') ?>", flashvars, params, attributes);
</script>

<img src="<?php echo application_asset_path('top_banner.jpg') ?>" />

<br class="clearfix" /><br />

<span class="heading_thanks blue">
    Thanks for your entry!
</span><br />

<span class="blue">
    you're now in the draw to win 1 of 14 Sony Ericsson Xperia&trade; arc Android smartphones!
</span> <br /><br />

<div class="left share_invite">
    <a href="javascript: invite_friends()"><img src="<?php echo application_asset_path('invite_friend.jpg') ?>" /></a>
</div>

<div class="right share_invite">
    <a href="javascript: share_to_wall()"><img src ="<?php echo application_asset_path('share_wall.jpg') ?>" /></a>
</div>

<br class="clearfix" />



<div class="main_screen_no_index">
    <div class="arc_phone">
<!--        <img class="image_question" src="<?php echo application_asset_path('thankyou_play.jpg') ?>" />-->
        <div id='container'>
                <div id='flashDIV'>
                </div>
        </div>
    </div>
    
    <div class="android_popcorn"></div>
    
    <div class="greenball_overlay"></div>
    
    <div class="xperia_label"></div>
</div>

<br class="clearfix" />

<div class="footer_technologies"></div>

<?php include_partial('arc/footer'); ?>

<script>
    function invite_friends() {
        FB.ui({method: 'apprequests', message: 'Sony Ericsson is capturing the True Colours of New Zealand with the incredible new Xperia arc. Correctly identify a New Zealand landmark and you could win 1 of 14 Xperia arc smartphones!',max_recipients: 1, data: "<?php echo $_GET['callback_data'] ?>"});
    }
    function share_to_wall() {
        FB.init({appId: '227423353963440', status: false, cookie: false,xfbml: true});

        FB.ui(
        {
            method: 'feed',
            name: 'Sony Ericsson Xperia Arc&trade; True Colours',
            link: 'https://www.facebook.com/sonyericssonnz?sk=app_227423353963440',
            picture: 'http://facebook.satelliteapps.co.nz/uploads/app/arc/share_new.jpg',
            caption: 'Sony Ericsson',
            description: 'Sony Ericsson is capturing the True Colours of New Zealand with the incredible new Xperia&trade; arc. Correctly identify a New Zealand landmark and you could win 1 of 14 Xperia&trade; arc smartphones!'
        });
    }
</script>