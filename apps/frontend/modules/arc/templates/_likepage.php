<link href='http://fonts.googleapis.com/css?family=Kreon' rel='stylesheet' type='text/css'>

<img src="<?php echo application_asset_path('top_banner.jpg') ?>" />

<img src="<?php echo application_asset_path('fangate_reversed.jpg') ?>" />

<br class="clearfix" />

<div class="main_screen">
    <div class="arc_phone">
        <?php
            // let's define the question
            $question_form = rand(1,6);
        ?>
        <img class="image_question" src="<?php echo application_asset_path('questions/0'. $question_form .'.jpg') ?>" />
    </div>
    
    <div class="android_popcorn"></div>
    
    <div class="greenball_overlay"></div>
    
    <div class="xperia_label"></div>
</div>

<!--<img src="<?php echo application_asset_path('splashscreen.jpg') ?>" />-->

<br class="clearfix" />

<div class="footer_technologies"></div>

<div class="foot_notes">
    This promotion is in no way sponsored, endorsed or administered by, or associated with, Facebook.  By entering this competition, you agree to our terms and conditions.
</div>