<div class="foot_notes">
    <a title="Terms and Conditions" class="fbalert" href="<?php echo application_landing_url('terms-and-conditions') ?>">Terms and Conditions</a> | <a title="Privacy Policy" class="fbalert" href="<?php echo application_landing_url('privacy-policy') ?>">Privacy Policy</a><br />
    This promotion is in no way sponsored, endorsed or administered by, or associated with, Facebook.  By entering this competition, you agree to our terms and conditions.
</div>