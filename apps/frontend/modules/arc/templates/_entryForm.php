<div class="blue" style="font-size: 21px; margin-bottom: 5px;">Question</div>
<div class="green" style="font-size: 17px;">Name one of the Kiwi landmarks shown in vivid true colour on the Xperia&trade; arc screen above</div><br />

<form action="<?php echo application_url($page) ?>" method="post" onsubmit="return checkfields()">

        <div class="form_row first_name_form_row">
        <span class="form_label">First Name:</span><br />
        <?php echo $form['first_name'] ?>
        </div>

        <div class="form_row surname_form_row">
        <span class="form_label">Last Name:</span><br />
        <?php echo $form['surname'] ?>
        </div>
    
        <div class="form_row surname_form_row">
        <span class="form_label">Email Address:</span><br />
        <?php echo $form['email'] ?>
        </div>
    
        <div class="form_row surname_form_row">
        <span class="form_label">Competition Answer:</span><br />
        <?php echo $form['answer'] ?>
        </div>
    
        <div class="form_row terms_form_row">
        <?php echo $form['terms'] ?>
                I have read and accept the <a title="Terms and Conditions" class="fbalert" href="<?php echo application_landing_url('terms-and-conditions') ?>">terms and conditions</a>
        </div>
    
        <div class="form_row terms_form_row_2">
            <input type="checkbox" id="privacy_popup_agree" name="xperia_arc[terms]">
            I have read and accept the <a title="Privacy Policy" class="fbalert" href="<?php echo application_landing_url('privacy-policy') ?>">privacy policy</a>
        </div>
  
        <div class="submit_button">
            <input  type="image" value="Submit" src="<?php echo application_asset_path('enter_button.jpg') ?>" />
        </div>
        
  
  <?php echo $form->renderHiddenFields() ?>
  
</form>

<?php

$answer_form[1] = "/^(devil\'s\sbath|devils\sbath|devil\'s\sbath\srotorua|devils\sbath\srotorua|wai\-o\-tapu)$/i";
$answer_form[2] = "/^(milford\ssound|milford\ssound\squeenstown)$/i";
$answer_form[3] = "/^(cape\sreinga\slighthouse|cape\sreinga)$/i";
$answer_form[4] = "/^(sky\stower|skytower|sky\stower\sauckland|skytower\sauckland)$/i";
$answer_form[5] = "/^(beehive|bee\shive|beehive\swellington|bee\shive\swellington)$/i";
$answer_form[6] = "/^(emerald\slakes|tongariro|emerald\slakes\stongariro|tongariro\snational\spark|emerald\slakes\stongariro\snational\spark)$/i";

?>

<script>
    
    function checkfields() {
        
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var title = "Xperia Arc";
        
        var answer_reg = <?php echo $answer_form[$question_form] ?>;
        
        if (!$('#xperia_arc_first_name').val()) {
            fbnofity_alt(title, "Please enter your first name");
            return false;
        } else if (!$('#xperia_arc_surname').val()) {
            fbnofity_alt(title, "Please enter your last name");
            return false;
        } else if (!$('#xperia_arc_email').val()) {
            fbnofity_alt(title, "Please enter your email address");
            return false;
        } else if (!emailReg.test($('#xperia_arc_email').val())) {
            fbnofity_alt(title, "Please check your email address");
            return false;
        } else if (!$('#xperia_arc_answer').val()) {
            fbnofity_alt(title, "Please enter your answer");
            return false;
        } else if (!$("#xperia_arc_terms").is(':checked')) {
            fbnofity_alt(title, "You must agree to Xperia Arc terms and conditions");
            return false;
        } else if (!$("#privacy_popup_agree").is(':checked')) {
            fbnofity_alt(title, "You must agree to Xperia Arc Privacy Policy");
            return false;
        } else if (!answer_reg.test($('#xperia_arc_answer').val())) {
            fbnofity_alt(title, "Sorry, that answer is incorrect");
            return false;
        } else {
            return true;
        }
        
    }
    
</script>

<?php if ($duplicate): ?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js"></script>

<script>
    $(document).ready(function() {
        fbnofity_alt("Sorry", "Sorry, you have already entered this competition");
    });
</script>
<?php endif; ?>
