<?php

class entryFormComponent extends sfComponent
{ 
  public function execute($request)
  {
            
    $this->application = $this->getRequest()->getAttribute('application');
    $this->page = $this->getRequest()->getAttribute('page');
    
    $this->form = new XperiaArcForm(null, array(), false);
    
    // see if the user has incoming apprequests
    $user = $this->getUser()->getFacebook()->getUser();
    
    if ($user) {
        $apprequests = $this->getUser()->getFacebook()->api("/me/apprequests");
        
        $q = Doctrine::getTable('XperiaArc')->createQuery('u')->where('u.accepted_ids = ?', $apprequests['data'][0]['id']);
        $position_id = $q->fetchOne();
        
        if (!$position_id->email AND $apprequests['data'][0]['data']) {
            Doctrine_Query::create()->update('XperiaArc u')->set('u.bonus', 'u.bonus + 1')->where('u.email = ?', $apprequests['data'][0]['data'])->execute();
        }
        
        if ($apprequests['data'][0]['data']) {
            Doctrine_Query::create()->update('XperiaArc u')->set('u.accepted_ids', $apprequests['data'][0]['id'])->where('u.email = ?', $apprequests['data'][0]['data'])->execute();
        }
        
    }
    
    if ($request->isMethod('post'))
    {
      if ($this->form->bindAndSave($request->getParameter($this->form->getName())))
      {
          $this->getController()->redirect(array('sf_route' => 'page', 'app_slug' => $this->application->slug, 'page_slug' => 'thanks', 'callback_data' => $_POST['xperia_arc']['email']));
      } else {
          if ($_POST['xperia_arc']['email']) {
              $this->duplicate = true;
          }
      }
    }
  }
}