<?php

class entryFormComponent extends sfComponent
{ 
  public function execute($request)
  {
      
    $this->application = $this->getRequest()->getAttribute('application');
    $this->page = $this->getRequest()->getAttribute('page');
    
    $this->form = new VodafoneMusicVotingEntryForm();
    
    if ($request->isMethod('post'))
    {
      if ($request->hasParameter($this->form->getName()) AND $this->form->bindAndSave($request->getParameter($this->form->getName())))
      {
          foreach( $this->getUser()->getAttribute('selectedTracks') as $track ){
            $vote = new VodafoneMusicVotingVote();
            $vote->track_id = $track;
            $vote->entry_id = $this->form->getObject()->id;
            $vote->save();
          }  
          
          $this->submitted = true;
      }
    }
  }
}