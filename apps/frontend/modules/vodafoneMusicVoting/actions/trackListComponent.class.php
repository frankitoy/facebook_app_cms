<?php

class trackListComponent extends sfComponent
{ 
  public function execute($request)
  {
    $this->application = $this->getRequest()->getAttribute('application');
    $this->page = $this->getRequest()->getAttribute('page');
    
    if( isset($this->show_top_ten) and $this->show_top_ten == true){
      $this->top10 = VodafoneMusicVotingTrack::getTopTen();    
    } else {
      $this->trackList = Doctrine::getTable('VodafoneMusicVotingTrack')->findAll();  
    }
    if( $request->hasParameter('trackList') and $request->isMethod(sfRequest::POST) ){
      $selectedTracks = @array_pop( $request->getParameter('trackList',array()));        
        
      if( count($selectedTracks) > 0 and count($selectedTracks) <= 10 ) {
        $this->getUser()->setAttribute('selectedTracks', $selectedTracks);
        $this->getController()->redirect(array('sf_route' => 'page', 'app_slug' => $this->application->slug, 'page_slug' => 'enter'));
      } else {
        $this->error = 'Please select at least one track and no more than 10';
      }
        
    }
     
  }
}      
    