<?php use_javascript('https://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js') ?>
<?php use_application_stylesheet('screen.css?v=2') ?>
<?php use_application_javascript('script.js') ?>

<div class="container">
  <div class="header" <?php echo isset($closed) ? 'style="height:198px"' : '' ?>>
    <img src="<?php echo application_asset_path('title.png') ?>" class="title" />
    <a onclick="window.open(this.href,'Vodafone Music Xmas','height=340,width=550');return false" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('http://play.vodafone.co.nz/Vodafone-Music-Xmas.html') ?>&t=<?php echo urlencode('Vodafone Music Xmas') ?>" class="share" >
      <img src="<?php echo application_asset_path('share.png') ?>" class="share" />
    </a>
    
    <?php if(!isset($closed)) : ?>
    <div class="copy">
   		 <?php if($show_top_ten) : ?>
    	<p>Your early Christmas present has arrived. The votes have been counted, so here are the top 10 NZ tracks as chosen by you.</p>
			<p>Thanks to the thousands of you that voted. You can download the tracks here, straight to your mobile from Vodafone live!, or via our Android-only store at <a href="http://m.vodafone.co.nz" target="_blank">m.vodafone.co.nz</a>. *Free for the month of December!</p> 
			<p>Happy Christmas!</p>
			<p class="small">*Data charges apply for all downloads.  Downloads require 'My Vodafone' access for consumer mobile customers only.  iPhone users can download to iTunes via PC. For more details please read the <a target="_blank" href="http://play.vodafone.co.nz/terms/music-voting">Terms &amp; Conditions</a>.</p>
    	<?php else: ?>  
      <p>Christmas carols driving you nuts? Vote for your favourite NZ tracks of all time and say no to Snoopy’s Xmas. Choose your favourite singles from the selection featured and we’ll make the 10 most popular tracks FREE throughout December.</p> 
      <p>As an added bonus – vote and you’ll go in the draw to win $250 of airtime.  The winning ten songs will be available for download from the 1st December.</p>
      <p class="small">Downloads are for Vodafone mobile customers only.<br/>
      Data charges and Terms and conditions apply.</p>
			<?php endif; ?>
    </div>
    <?php endif; ?>
    
  </div>
  <img src="<?php echo application_asset_path('albums.jpg') ?>" style="display: block" />
  <div class="main_container">
    <?php if(isset($closed) and $closed == true): ?>
      <img src="<?php echo application_asset_path('thanks.jpg') ?>" style="display: block; margin: 36px auto;" />  
    <?php else: ?>      
      <?php include_component('vodafoneMusicVoting', 'trackList', array('show_top_ten' => isset($show_top_ten)?$show_top_ten:false )); ?>
    <?php endif; ?>
  </div>
</div>
<div class="footer_notes">
    &copy; 2011 Vodafone New Zealand Ltd. | <a href="http://www/vodafone.co.nz" target="_blank">vodafone.co.nz</a> | <a target="_blank" href="http://play.vodafone.co.nz/terms/music-voting">Terms &amp; Conditions</a> | <a target="_blank" href="http://www.vodafone.co.nz/about/legal-stuff/vodafone-privacy-policy.jsp">Privacy Policy</a> <br /><br />
    This promotion is in no way sponsored, endorsed or administered by, or associated with, Facebook. 
    By entering this competition, you agree to our <a target="_blank" href="http://play.vodafone.co.nz/terms/music-voting">terms &amp; conditions</a>
</div>