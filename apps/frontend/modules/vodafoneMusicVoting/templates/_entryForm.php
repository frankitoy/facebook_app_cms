<div class="entry-form-wrapper">
<div id="entryForm">
  <a href="<?php echo url_for(array('sf_route' => 'page', 'app_slug' => $application->slug, 'page_slug' => 'vote')) ?>"><img src="<?php echo application_asset_path('close.png') ?>" alt="Close" class="close" ></a>
  <?php if(isset($submitted) and $submitted == true): ?>
    <h2>Thanks for voting!</h2>
  <?php else: ?>
    <h2>Give us your details to enter the competition</h2>
    <?php if($form->hasGlobalErrors()): ?>
    <div class="error_note" id="global_error">
        <?php foreach($form->getGlobalErrors() as $err): ?>
          <?php echo $err."<br>"; ?>
        <?php endforeach; ?> 
    </div> 
    <?php endif; ?>
    <div class="error_note" id="error" style="display: none">
        fill up all the fields
    </div>
    <form action="<?php echo application_url($page) ?>" method="post">
        <table>
        <?php echo $form['name']->renderRow(array(),'Name') ?>
        <?php echo $form['mobile']->renderRow(array(),'Mobile number') ?>
        <?php echo $form['email']->renderRow(array(),'Email address') ?>
        <?php echo $form['confirm_email']->renderRow(array(),'Confirm email address') ?>
        <tr>
          <td colspan="2" style="height:15px"></td>
        </tr>
        <tr>
          <td colspan="2" class="inline">
            Sign up to receive Vodafone Music alerts<br>
            <?php echo $form['music_alert_email']->render() ?><?php echo $form['music_alert_email']->renderLabel('Email Me') ?>
            <?php echo $form['music_alert_text']->render() ?><?php echo $form['music_alert_text']->renderLabel('Text Me') ?>
          </td>
        </tr>
        <tr>
          <td colspan="2" style="height:10px"></td>
        </tr>
        <tr>
          <td colspan="2" class="inline">
            Let me know when my &lsquo;top 10 downloads&rsquo; Christmas gift has arrived.<br/>
            <?php echo $form['notify']->render() ?><?php echo $form['notify']->renderLabel('Yes please!') ?>
          </td>
        </tr>
        <tr>
          <td colspan="2" style="height:10px"></td>
        </tr>
        <tr>
          <td colspan="2" class="inline">
            <?php echo $form['accept_tc']->render() ?>
            <label for="vodafone_music_voting_entry_accept_tc">I accept the <a href="http://play.vodafone.co.nz/terms/music-voting" target="_blank">terms &amp; conditions</a></label>
            <?php echo $form['accept_tc']->renderError() ?>
          </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <input type="image" src="<?php echo application_asset_path('button-submit.png') ?>" name="submit" />
            </td>
        </tr>
        </table>
        <?php echo $form->renderHiddenFields(); ?>
        
        
    </form>
  
  <?php endif; ?>
</div>

<div class="cover"></div>

<?php include_partial('vodafoneMusicVoting/entrypage')  ?>
</div>