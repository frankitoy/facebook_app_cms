<div id="trackList">
<?php if( isset($show_top_ten) and $show_top_ten == true): ?>
  <div class="top10">
    <h2>Here are your top 10 tracks</h2>
    <div class="clearfix">
    <?php 
      $i=1;
      foreach($top10 as $track): ?>
      <div class="track <?php echo $i%2!=0 ?'odd':'' ?> " id="track_<?php echo $track->track_id ?>">
        <div class="number">Track <?php echo $i ?>:</div>
        <div class="trackname"><?php echo $track->artist ?> &mdash; <?php echo $track->track ?> </div>
      </div>  
    <?php 
      $i++;
      endforeach; ?>
      </div>
      <a href="http://play.vodafone.co.nz/free-xmas-download" target="_blank">
        <img src="<?php echo application_asset_path('button-download.png') ?>" class="download" />
      </a>       
  </div>

<?php else: ?>
  
  <?php if(isset($error)): ?>
    <p><?php echo $error ?></p>
  <?php endif; ?>
  <form action="<?php echo application_url($page) ?>" method="post" onsubmit="return checkTrackListFields()">
    <?php if($trackList->count() == 0): ?>
      <p>No tracks available</p>  
    <?php else: ?>
      <div class="clearfix">
    <?php 
      $i=1;
      foreach($trackList as $track): ?>
      <div class="track <?php echo $i%2!=0 ?'odd':'' ?> " id="track_<?php echo $track->track_id ?>">
        <div class="number">Track <?php echo $i ?>:</div>
        <div class="trackname"><?php echo $track->artist ?> &mdash; <?php echo $track->track ?> </div>
        <div class="controls">
          <?php if($track->has_preview): ?>
          <div class="control_button play" data-trackid="<?php echo $track->track_id ?>"></div>
          <div class="control_button buffering"><img src="<?php echo application_asset_path('loading.gif') ?>" alt="Loading..."></div>
          <div class="control_button stop"><img src="<?php echo application_asset_path('stop.png') ?>" alt="Stop"></div>
          <div class="control_button playing"><img src="<?php echo application_asset_path('eq.gif') ?>" alt="Playing"></div>
          <?php else: ?>
            <img src="<?php echo application_asset_path('no-preview.png') ?>" alt="No Preview available" title="No Preview available">
          <?php endif; ?>
        </div>
        <div class="vote">
          <label for="trackList_track_<?php echo $track->id ?>">Vote</label>
          <input type="checkbox" value="<?php echo $track->id ?>" name="trackList[tracks][]" id="trackList_track_<?php echo $track->id ?>">
        </div>      
      </div>  
    <?php 
      $i++;
      endforeach; ?>
      </div>
    <?php endif; ?>
    <div class="button">
      <input type="image" src="<?php echo application_asset_path('button-send-your-vote.png') ?>" nam="submit" />  
    </div>
    
  </form>
  <div id="music_player_layer"> 
    <div id="music_player"></div> 
  </div>
    
  <script type="text/javascript"> 
    var xiSwfUrlStr = "<?php echo application_asset_path('player/playerProductInstall.swf') ?>";
    var playerUrl = "<?php echo application_asset_path('player/player.swf') ?>";
  </script>

<?php endif; ?>

</div>

