<?php

class shortlandStreetSpecialActions extends sfActions
{
  private $application;
  
  public function preExecute()
  {
    $this->application = $this->getUser()->getApplication();
    
    $this->setTemplate('creditsCallback');
  }
  
  public function executePaymentsGetItems(sfWebRequest $request)
  {
    $product = sfConfig::get(sprintf('app_%s_product', $this->application->app_id));
    
    if (!is_array($product))
    {
      throw new sfException('Could not find product.');
    }
    
    $sr = $this->getUser()->getFacebook()->getSignedRequest();
    
    $payload = $sr['credits'];
    
    $form = new ShortlandStreetSpecialPurchaseLogForm(null, array(), false);
    
    $formData = array(
      'order_id' => $payload['order_id'],
      'facebook_id' => $payload['buyer'],
      'is_test' => (boolean) $payload['test_mode'],
      'status' => ShortlandStreetSpecialPurchaseLogTable::STATUS_PLACED
    );
    
    if (!$form->bindAndSave($formData))
    {
      throw $form->getErrorSchema();
    }
    
    $this->responseData = array(
      'method' => 'payments_get_items',
      'content' => array($product)
    );
  }
  
  public function executePaymentsStatusUpdate(sfWebRequest $request)
  {
    $sr = $this->getUser()->getFacebook()->getSignedRequest();
    
    $payload = $sr['credits'];
    
    $purchase = Doctrine::getTable('ShortlandStreetSpecialPurchaseLog')->findOneByOrderId($payload['order_id']);
    
    $this->forward404Unless($purchase, 'Could not find purchase by order id ' . $payload['order_id']);
    $this->forward404Unless($purchase->status === $payload['status'], 'Purchase status does not match payload status ' . $payload['order_id']);
    
    switch ($payload['status'])
    {
      case ShortlandStreetSpecialPurchaseLogTable::STATUS_PLACED:
        $purchase->status = $purchase->status = ShortlandStreetSpecialPurchaseLogTable::STATUS_SETTLED;
        break;
      
      default:
        $purchase->status = $payload['status'];
        break;
    }
    
    $purchase->save();
    
    $this->responseData = array(
      'method' => 'payments_status_update',
      'content' => array(
        'status' => $purchase->status,
        'order_id' => $purchase->order_id
      )
    );
  }
}
