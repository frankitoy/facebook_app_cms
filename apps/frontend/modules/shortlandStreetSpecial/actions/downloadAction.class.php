<?php

class downloadAction extends sfAction
{
  public function execute($request)
  {
    $application = Doctrine::getTable('Application')->findOneBySlug('shortland-street-special');
    $this->forward404Unless($application);
    
    $files = sfConfig::get(sprintf('app_%s_downloads', $application->app_id), array());
    
    $accessToken = $request->getParameter('access_token');
    $format = $request->getParameter('format');
    $this->forward404Unless($accessToken && $format && array_key_exists($format, $files));
    
    $purchase = Doctrine::getTable('ShortlandStreetSpecialPurchaseLog')->findOneByAccessToken($accessToken);
    $this->forward404Unless($purchase);
    
    $this->getUser()->setApplication($application);
    
    $facebookId = $this->getUser()->getFacebookId();
    $this->forward404Unless($facebookId == $purchase->facebook_id);
    
    $file = sfConfig::get('sf_data_dir') . '/shortland-street-special/' . $files[$format];

    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($file).'"');
    header('Content-Length: '.filesize($file));
    
    set_time_limit(0);
    
    if (false === ($fd = fopen($file, 'rb')))
    {
      throw new sfException('Could not open download file.');
    }
    
    while (!feof($fd))
    {
      print fread($fd, 1024*16);
      ob_flush();
      flush();
    }
  }
}
