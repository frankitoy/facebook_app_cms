<?php

class downloadListComponent extends sfComponent
{
  public function execute($request)
  {
    $isConnected = $this->getUser()->isFacebookConnected();
    
    if ($isConnected)
    {
      $facebookId = $this->getUser()->getFacebookId();
      
      $purchase = Doctrine::getTable('ShortlandStreetSpecialPurchaseLog')->getPurchaseForUser($facebookId);
    }
    else
    {
      $purchase = false;
    }
    
    if (!($isConnected && $purchase))
    {
      $application = $this->getUser()->getApplication();
      
      $this->getController()->redirect(array('sf_route' => 'page', 'app_slug' => $application->slug, 'page_slug' => 'home', 'purchase' => 1));
    }
    
    $this->accessToken = $purchase->access_token;
  }
}