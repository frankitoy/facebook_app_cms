<ul class="panels clearfix">
  <li>
    <h2>How it works</h2>
    <p>Use facebook credits to buy the episode. You can purchase Facebook Credits using your credit card, PayPal, a mobile phone and many other alternative payment methods. To buy Facebook Credits or check your balance, go to the <a href="https://www.facebook.com/editaccount.php?payments" target="_top">Payments</a> tab in your Account Settings. Once downloaded the episode is yours to keep. Please note additional charges apply if you purchase Facebook credits using your mobile phone.</p>
  </li>
  <li class="last">
    <h2>Download options</h2>
    <p>Simply choose the link that best suits your needs. The Feature length episode is available for Mac, PC, iphone, ipad, Android and Android tab.</p>
    <p>By clicking the buy button, you confirm you have read the <a href="<?php echo application_url('license') ?>" class="facebook_popup">Download License</a>.</p>
  </li>
</ul>