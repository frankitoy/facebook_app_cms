<ul class="download_list clearfix">
  <li>
    <h4>Computer</h4>
    <ul>
      <li class="quicktime">
        Quicktime<br />
        <a href="<?php echo url_for(array('sf_route' => 'shortland_street_special_download', 'access_token' => $accessToken, 'format' => 'quicktime_hd')) ?>">HD</a>
        <a href="<?php echo url_for(array('sf_route' => 'shortland_street_special_download', 'access_token' => $accessToken, 'format' => 'quicktime_sd')) ?>">Standard Definition</a>
      </li>
      <li class="windowsmedia">
        Windows Media<br />
        <a href="<?php echo url_for(array('sf_route' => 'shortland_street_special_download', 'access_token' => $accessToken, 'format' => 'windowsmedia_hd')) ?>">HD</a>
        <a href="<?php echo url_for(array('sf_route' => 'shortland_street_special_download', 'access_token' => $accessToken, 'format' => 'windowsmedia_sd')) ?>">Standard Definition</a>
      </li>
      <li class="avi">
        AVI<br />
        <a href="<?php echo url_for(array('sf_route' => 'shortland_street_special_download', 'access_token' => $accessToken, 'format' => 'avi_hd')) ?>">HD</a>
        <a href="<?php echo url_for(array('sf_route' => 'shortland_street_special_download', 'access_token' => $accessToken, 'format' => 'avi_sd')) ?>">Standard Definition</a>
      </li>
    </ul>
  </li>
  <li class="last">
    <h4>Mobile/Tablet</h4>
    <ul>
      <li class="apple">
        iPhone/iPod/iPad<br />
        <a href="<?php echo url_for(array('sf_route' => 'shortland_street_special_download', 'access_token' => $accessToken, 'format' => 'apple_lg')) ?>">Large</a>
        <a href="<?php echo url_for(array('sf_route' => 'shortland_street_special_download', 'access_token' => $accessToken, 'format' => 'apple_sm')) ?>">Small</a>
      </li>
      <li class="android">
        Android Mobile/Tablet<br />
        <a href="<?php echo url_for(array('sf_route' => 'shortland_street_special_download', 'access_token' => $accessToken, 'format' => 'android_lg')) ?>">Large</a>
        <a href="<?php echo url_for(array('sf_route' => 'shortland_street_special_download', 'access_token' => $accessToken, 'format' => 'android_sm')) ?>">Small</a>
      </li>
    </ul>
  </li>
</ul>
