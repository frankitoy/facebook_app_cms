<form action="<?php echo application_url($page) ?>" method="post" onsubmit="return checkfields()">
    <table width="90%">
    <tr>
        <td width="100">Your Name</td>
        <td><?php echo $form['name'] ?></td>
    </tr>

    <tr>
        <td>Email</td>
        <td><?php echo $form['email'] ?></td>
    </tr>

    <tr>
        <td>Phone</td>
        <td><?php echo $form['phone'] ?></td>
    </tr>

    <tr>
        <td>Answer</td>
        <td><?php echo $form['answer'] ?></td>
    </tr>

    <tr>
        <td colspan="2">
            <input type="checkbox" name="agree_conditions" id="agree_terms" /> I have read and agree to the <a target="_blank" href="http://play.vodafone.co.nz/terms/better-value">Terms and Conditions</a>
        </td>
    </tr>

    <tr>
        <td colspan="2" height="50">
            <input type="image" src="<?php echo application_asset_path('button_entertowin.jpg') ?>" style="float: left" />
            
            <div class="error_note" id="error" style="display: none">
                fill up all the fields
            </div>
            
        </td>
    </tr>
    </table>
</form>

<script>
    
    function checkfields() {
    
        $("#error").html("");
        
        if (!$('#vodafone200_name').val()) {
            $("#error").html("Please enter your name").fadeIn("fast");
            return false;
        } else if (!$('#vodafone200_email').val()) {
            $("#error").html("Please enter your email address").fadeIn("fast");
            return false;
        } else if (!$('#vodafone200_phone').val()) {
            $("#error").html("Please enter your phone number").fadeIn("fast");
            return false;
        } else if (!$('#vodafone200_answer').val()) {
            $("#error").html("Please enter your answer").fadeIn("fast");
            return false;
        } else if (!$("#agree_terms").is(':checked')) {
            $("#error").html("You must agree to Vodafone Terms and Conditions").fadeIn("fast");
            return false;
        } else {
            return true;
        }
        
    }
    
</script>