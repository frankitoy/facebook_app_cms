<div class="container">
        
        <div class="white_bg">
            <div class="top_header">
                <a href="http://www.facebook.com/sharer.php?u=http://play.vodafone.co.nz/vodafone-200-value.html" target="blank">
                    <img src="<?php echo application_asset_path('header.jpg') ?>" />
                </a>
            </div>
            <div class="youtube_container">
<!--                <img src="<?php echo application_asset_path('youtube_overlay.jpg') ?>" width="501" />-->
                <iframe width="501" height="336" src="http://www.youtube.com/embed/4SSE7545tUA" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    
        <div class="content_gray_holder">
            <div class="main_container">
                <div class="margin_separator">
                    <table width="100%">
                        <tr>
                            <td width="50%" valign="top" style="border-right: 1px solid #CCC;">
                                <div class="header_newsmartplans"></div>
                                <div class="limited_width limited_lineheight">
                                    <p>
                                        No matter how you use your mobile, we've got a <a target="_blank" href="http://www.vodafone.co.nz/phones-plans/plans/smart/">range of great value mobile plans</a> to choose from. TXTs to any NZ mobile and minutes to any NZ mobile or landline, along with plenty of data, all for a great monthly price.
                                    </p>

                                    <br />

                                    <p>
                                        We also have <a target="_blank" href="http://www.vodafone.co.nz/home-phone/">great value home phones</a> and <a target="_blank" href="http://www.vodafone.co.nz/internet/">broadband options</a> to suit every type of household. Whether you're online every day, or just browse the web occasionally, we've got a device and plan to suit you.
                                    </p>

                                    <br />

                                    <p>
                                        Plus, get one of our great <a target="_blank" href="http://www.vodafone.co.nz/entertainment/tv/">SKY with Vodafone</a> discounts when you have a selected fixed line broadband plan and SKY with Vodafone.
                                    </p>
                                </div>
                            </td>

                            <td width="50%" valign="top">
                                <div class="limited_width right_aligned">
                                    <div class="header_win2000"></div><br />
                                    <p>
                                        Enter now by watching the video and answering the question below.  
                                    </p>

                                    <br />

                                    <p>
                                        We'll put you in the draw to win a $2,000 credit* on your Vodafone mobile or fixed line account. Don't miss out!
                                    </p>

                                    <br />

                                    <p class="small_note">
                                        * cannot be used for devices or hardware.
                                    </p>

                                    <br /><br />

                                    <div class="grey_box">
                                        <strong>Q.</strong> How much does the Smart Plan featured in the video cost per month?
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>

                    <br />

                    <div class="header_entertowin"></div>

                    <div class="cellphone"></div>
                </div>
            </div>
            <div class="redbox">
                <div class="rb_margin_sep">
                    
                    <img src="<?php echo application_asset_path('thankyou_note.jpg') ?>" />

                </div>
            </div>

            <div class="footer_notes">
                2011 Vodafone New Zealand Ltd. | <a style="text-decoration: none; color: #696969" target="_blank" href="http://www.vodafone.co.nz">vodafone.co.nz</a> | <a style="text-decoration: none; color: #696969" target="_blank" href="http://play.vodafone.co.nz/terms/better-value">Terms & Conditions</a> | <a style="text-decoration: none; color: #696969" target="_blank" href="http://play.vodafone.co.nz/privacy-policy">Privacy Policy</a> <br /><br />
                This promotion is in no way sponsored, endorsed or administered by, or associated with, Facebook. 
                By entering this competition, you agree to our terms & conditions
            </div>
        </div>
    </div>

