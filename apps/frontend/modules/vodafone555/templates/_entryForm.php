<h2>Entry Form</h2>
<?php if($form->hasGlobalErrors()): ?>
<div class="error_note" id="global_error">
    <?php foreach($form->getGlobalErrors() as $err): ?>
      <?php echo $err."<br>"; ?>
    <?php endforeach; ?> 
</div> 
<?php endif; ?>
<div class="error_note" id="error" style="display: none">
    fill up all the fields
</div>
<form action="<?php echo application_url($page) ?>" method="post" onsubmit="return checkfields()">
    <table width="90%">
    <?php echo $form['name']->renderRow(array(),'Your Name') ?>
    <?php echo $form['email']->renderRow() ?>
    <?php echo $form['phone']->renderRow() ?>
    <?php echo $form['answer']->renderRow() ?>

    <tr>
        <td colspan="2" class="terms">
            <input type="checkbox" name="agree_conditions" id="agree_terms" /><label for="agree_terms">I have read and agree to the <a target="_blank" href="http://play.vodafone.co.nz/terms/xmas-vodafone555-blue">Terms and Conditions</a></label> 
        </td>
    </tr>

    <tr>
        <td colspan="2" height="50">
            <button type="submit"><img src="<?php echo application_asset_path('button.png') ?>" /></button>
        </td>
    </tr>
    </table>
</form>

<script>
    
    function checkfields() {
    
        $("#error").html("");
        var strText = jQuery.trim($('#vodafone555_answer').val());
        
        if (!$('#vodafone555_name').val()) {
            $("#error").html("Please enter your name").fadeIn("fast");
            return false;
        } else if (!$('#vodafone555_email').val()) {
            $("#error").html("Please enter your email address").fadeIn("fast");
            return false;
        } else if (!$('#vodafone555_phone').val()) {
            $("#error").html("Please enter your phone number").fadeIn("fast");
            return false;
        } else if (!$('#vodafone555_answer').val()) {
            $("#error").html("Please enter your answer").fadeIn("fast");
            return false;
        } else if (!$("#agree_terms").is(':checked')) {
            $("#error").html("You must agree to Vodafone Terms and Conditions").fadeIn("fast");
            return false;
        } else if (strText.split(/\s+/).length > 150) {
            $("#error").html("Your answer must be 150 words or less").fadeIn("fast");
            return false;
        } else {
            return true;
        }
        
    }
    
</script>