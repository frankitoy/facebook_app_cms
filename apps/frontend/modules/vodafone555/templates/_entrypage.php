<?php use_application_stylesheet('screen');
  use_javascript(application_asset_path('script.js'));
 ?>
<div class="container redback">
  <div class="top">
    <div id="header">
      <div class="header_copy"><p>It&rsquo;s like a present in a present. <a href="http://www.vodafone.co.nz/shop/mobileDetails.jsp?skuId=sku13240061&voucherCode=&selectionKey=mobile&menuKey=mnit700033" target="_blank">Get It here</a>.</p></div>
      <a onclick="window.open(this.href,'Vodafone 555','height=340,width=550');return false" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('http://facebook.satelliteapps.co.nz/2/share') ?>&t=<?php echo urlencode('Vodafone 555 Blue') ?>" class="share" >
        <img src="<?php echo application_asset_path('share.png') ?>" /></a>
    </div>  
    <div class="youtube_container">
        <div class="image"><img src="<?php echo application_asset_path('video-cover.jpg') ?>" /></div>
        <div class="video"></div>
    </div>
  </div>
  <div class="copy">
    <div class="inner">
      <p class="top em">This Christmas we&rsquo;re celebrating you! Purchase the new Vodafone 555 Blue this Christmas and you&rsquo;ll get a $30 top up for FREE!!</p>
      <p class="left"><span class="em">Win a Vodafone 555 Blue for an early Christmas pressie!</span><br>Simply tell us in 150 words or less who your favourite Facebook friend is and why? We’ll put you in the draw to win 1 of 10 Vodafone 555 Blue phones.</p>  
    </div>
    
  </div>        
   
   <div id="form">
     <?php if(isset($submitted) and $submitted == true): ?>
      <p class="thanks">Thanks, you&rsquo;re now in the draw to win</p>
     <?php else: ?>
     <?php include_component('vodafone555', 'entryForm') ?>
     <?php endif; ?>     
   </div>
    
</div>
<div class="footer_notes">
    2011 Vodafone New Zealand Ltd. | <a href="http://www/vodafone.co.nz" target="_blank">vodafone.co.nz</a> | <a target="_blank" href="http://play.vodafone.co.nz/terms/xmas-vodafone555-blue">Terms & Conditions</a> | <a target="_blank" href="http://www.vodafone.co.nz/about/legal-stuff/vodafone-privacy-policy.jsp">Privacy Policy</a> <br /><br />
    This promotion is in no way sponsored, endorsed or administered by, or associated with, Facebook. 
    By entering this competition, you agree to our <a target="_blank" href="http://play.vodafone.co.nz/terms/xmas-vodafone555-blue">terms & conditions</a>
</div>