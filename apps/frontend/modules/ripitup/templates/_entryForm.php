<br clear="all">
<div class="req_field" id="requirements"></div>
<form action="<?php echo application_url($page) ?>" method="post" onsubmit="return checkfields()">

<table>

  <?php echo $form['name']->renderRow(null,'Full Name:') ?>
  <?php echo $form['email']->renderRow(null,'Email Address:') ?>
  <?php echo $form['add_mailing_list']->renderRow(null,'Sign me up to the Rip It Up newsletter:') ?>
  <?php echo $form['add_edge_mailing_list']->renderRow(null,'Sign me up to The Edge newsletter:') ?>
  <?php echo $form['add_label_mailing_list']->renderRow(null,'Sign me up to the umusic newsletter:') ?>

  <tr>
    <th>
      <label for="agree_terms">I have read and agree to the <a href="http://ripitup.co.nz/contentitem/terms-and-conditions-jessie-j-competition/3028" target="_blank">Terms &amp; Conditions</a></label>
    </th>
    <td>
      <input type="checkbox" id="agree_terms" />
    </td>
    <tr>
      <td></td>
      <td><input type="submit" value="Enter!"/></td>
    </tr>
  </tr>
</table>

</form>



<script>
    
    function checkfields() {
    
        var title = "Jessie J Competition";
        
        if (!$('#ripitup_name').val()) {
            $("#requirements").html("Please enter your name").fadeIn("fast");
            return false;
        } else if (!$('#ripitup_email').val()) {
            $("#requirements").html("Please enter your email address").fadeIn("fast");
            return false;
        } else if (!$("#agree_terms").is(':checked')) {
            $("#requirements").html("Please read Terms & Conditions").fadeIn("fast");
            return false;
        } else {
            return true;
        }
        
    }
    
</script>