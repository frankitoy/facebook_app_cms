<?php

$entries = $entries->getRawValue();

$filename = tempnam(sys_get_temp_dir(), '');

$fp = fopen($filename, 'w');

fputcsv($fp, array(
  'First Name',
  'Surname',
  'Email',
  'Answer',
  'Bonus',
  'Entry Date'
));

foreach ($entries as $entry)
{
    $bonus = "";
    if ($entry['bonus']) {
        $bonus = "yes";
    }
    
  fputcsv($fp, array(
    $entry['first_name'],
    $entry['surname'],
    $entry['email'],
    $entry['answer'],
    $bonus,
    $entry['entry_date']
  ));
}

fclose($fp);

echo file_get_contents($filename);

unlink ($filename);
