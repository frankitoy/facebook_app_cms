<br clear="all">
<div class="req_field" id="requirements"></div>
<form action="<?php echo application_url($page) ?>" method="post" onsubmit="return checkfields()">

<table>

  <?php echo $form['name']->renderRow() ?>
  <?php echo $form['email']->renderRow() ?>
  
  <tr>
    <td colspan="2">
			<?php echo $form['add_mailing_list']->renderLabel('Sign me up to the Rip It Up newsletter') ?> &nbsp;&nbsp; <?php echo $form['add_mailing_list'] ?>
			<?php echo $form['add_mailing_list']->renderError() ?>      
    </td>
  </tr>  
  
  <tr>
    <td colspan="2">
			<label for="agree_terms">I have read and agree to the <a href="http://ripitup.co.nz/static-content/terms-and-condtions" target="_blank">Terms &amp; Conditions</a></label> &nbsp;&nbsp; <input type="checkbox" id="agree_terms" />      
    </td>
  </tr>  
  <tr>
    <td colspan="2">
			<button type="submit"><img src="<?php echo application_asset_path('summer-mixtape/submit.png') ?>" /></button>
    </td>
  </tr>
</table>
    
    
</form>



<script>
    
    function checkfields() {
    
        var title = "Gin Wigmore Competition";
        
        if (!$('#ripitup_name').val()) {
            $("#requirements").html("Please enter your name").fadeIn("fast");
            return false;
        } else if (!$('#ripitup_email').val()) {
            $("#requirements").html("Please enter your email address").fadeIn("fast");
            return false;
        } else if (!$("#agree_terms").is(':checked')) {
            $("#requirements").html("Please read and agree to the Terms & Conditions").fadeIn("fast");
            return false;
        } else {
            return true;
        }
        
    }
    
</script>