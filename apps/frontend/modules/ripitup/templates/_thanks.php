<?php use_application_stylesheet('main_jessej.css') ?>
<div class="thanks">
  <img class="image" src="<?php echo application_asset_path('jessej/thanks.jpg') ?>" />
  <div class="share">
    <a href="https://www.facebook.com/sharer.php?u=<?php echo urlencode('http://facebook.satelliteapps.co.nz/7/share') ?>" onclick="window.open(this.href,'Share','resizable=1,width=600,height=300'); return false">
      <img src="/images/share.png" alt="Share!">
    </a>
  </div>
</div>
