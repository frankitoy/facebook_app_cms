<?php 

class galleryComponent extends sfComponent
{
  
  public function execute($request)
  {
    $this->gallery = Doctrine::getTable('VodafoneN9')->getGallery();   
    
    $dirs = sfConfig::get('app_vodafone_nokia_n9_upload');          
    $this->large_path = '/uploads'.$dirs['folder'].$dirs['large'].'/';  
    $this->thumb_path = '/uploads'.$dirs['folder'].$dirs['thumb'].'/';  
  }
  
}


