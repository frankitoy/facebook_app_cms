<?php

class entryFormComponent extends sfComponent
{ 
  public function execute($request)
  {
    $dirs = sfConfig::get('app_vodafone_nokia_n9_upload');          
    $large_path = sfConfig::get('sf_upload_dir').$dirs['folder'].$dirs['large'].'/';  
    $thumb_path = sfConfig::get('sf_upload_dir').$dirs['folder'].$dirs['thumb'].'/';  
        
    $this->application = $this->getRequest()->getAttribute('application');
    $this->page = $this->getRequest()->getAttribute('page');
    
    $this->form = new VodafoneN9Form();
    
    if ($request->isMethod('post') and $request->hasParameter($this->form->getName()))
    {
       
      if ($this->form->bindAndSave( $request->getParameter($this->form->getName()), $request->getFiles($this->form->getName()) ))
      {
         
        $validatedFile = $this->form->getValue('photo');
        $savedFilename = basename($validatedFile->getSavedName());
        
        //Save large image
        $largeImage = new sfImage($validatedFile->getSavedName(),$validatedFile->getType(),'ImageMagick');
        $largeImage->resize(null,250, false, true);
        //$largeImage->setQuality(80);
        $largeImage->saveAs($large_path.$savedFilename);

        //Save thumb image                    
        $thumbImage = new sfImage($validatedFile->getSavedName(),$validatedFile->getType(),'ImageMagick');
        $thumbImage->thumbnail(140,81,'center');
        $thumbImage->setQuality(60);
        $thumbImage->saveAs($thumb_path.$savedFilename);
                  
        $this->getController()->redirect(array('sf_route' => 'page', 'app_slug' => $this->application->slug, 'page_slug' => 'thank-you'));
      
      }
    }
  }
}