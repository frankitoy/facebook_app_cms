<?php 
  use_javascript('jquery.history.min.js');
  use_javascript('jquery.galleriffic.min.js');
  use_javascript('jquery.opacityrollover.min.js');
  use_javascript('/uploads/app/vodafone-nokia-n9/gallery.js');
  use_stylesheet('galleriffic.css');
 ?>
<?php if($gallery->count() > 0): ?>
  
<div class="content">
  <div class="slideshow-container">
    <div id="loading" class="loader"></div>
    <div id="slideshow" class="slideshow"></div>
  </div>
  <div class="photo-index"></div>
</div> 
 
<div class="navigation-container">
  <div id="thumbs" class="navigation">
    <a class="pageLink prev" style="visibility: hidden;" href="#" title="Previous Page">
      <img src="<?php echo application_asset_path('scroll-left.png') ?>" width="20" height="81">
    </a>
    <ul class="thumbs">
      <?php foreach($gallery as $item): ?>
        <li>
          <a href="<?php echo $large_path . $item->photo ?>" class="thumb" name="img<?php echo $item->id ?>">
            <img src="<?php echo $thumb_path . $item->photo ?>" />
          </a>
        </li>
      <?php endforeach; ?>      
    </ul>
    <a class="pageLink next" style="visibility: hidden;" href="#" title="Next Page">
      <img src="<?php echo application_asset_path('scroll-right.png') ?>" width="20" height="81">
    </a>
  </div>
</div>

<?php else: ?>
  <p>No photos at the moment, but check back soon!</p>
<?php endif; ?>
 
 
 
 
 
 
 
 
 
 
<?php /* 
<div id="gallery">
  <?php if($gallery->count() > 0): ?>
  <div class="slideshow-container">
    <div id="controls" class="controls"></div>
    <div id="loading" class="loader"></div>
    <div id="large" class="slideshow">
      <img src="<?php echo $large_path . $gallery->getFirst()->photo ?>" />    
    </div>
  </div>
  
  <div id="scrollwrap">
    <img src="<?php echo application_asset_path('scroll-left.png') ?>" width="20" height="81" alt="Scroll left">
    <div id="scroller">
      <ul id="scroll_list">
        <?php foreach($gallery as $item): ?>
          <li>
            <a href="<?php echo $large_path . $item->photo ?>" class="thumb" name="IMG<?php echo $item->id ?>">
              <img src="<?php echo $thumb_path . $item->photo ?>" />
            </a>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
    <img src="<?php echo application_asset_path('scroll-right.png') ?>" width="20" height="81">
  </div> 
  
   <?php else: ?>
  <p>No photos at the moment, but check back soon!</p>
  <?php endif; ?>          
</div>

*/ ?>