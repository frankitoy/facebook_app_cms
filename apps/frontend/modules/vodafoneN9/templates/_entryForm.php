<?php if($form->hasGlobalErrors()): ?>
<div class="error_note" id="global_error">
    <?php foreach($form->getGlobalErrors() as $err): ?>
      <?php echo $err."<br>"; ?>
    <?php endforeach; ?> 
</div> 
<?php endif; ?>
<div class="error_note" id="error" style="display: none">
    fill up all the fields
</div> 
<form action="<?php echo application_url($page) ?>" method="post" onsubmit="return checkfields()" enctype="multipart/form-data">
    <table width="90%">
    <tr>
        <td width="100"><?php echo $form['name']->renderLabel() ?></td>
        <td>
          <?php echo $form['name']->render(); echo $form['name']->renderError() ?>
        </td>
    </tr>
   
    <tr>
        <td><?php echo $form['email']->renderLabel() ?></td>
        <td><?php echo $form['email']->render() ?><?php echo $form['email']->renderError() ?></td>
    </tr>

    <tr>
        <td><?php echo $form['phone']->renderLabel() ?></td>
        <td><?php echo $form['phone']->render() ?><?php echo $form['phone']->renderError() ?></td>
    </tr>

    <tr>
        <td><?php echo $form['photo']->renderLabel('Browse &amp; attach') ?></td>
        <td><?php echo $form['photo']->render() ?><?php echo $form['photo']->renderError() ?>
          <p class="small">Max filesize is 2MB. Only JPG, PNG, GIF files are permitted.</p></td>
    </tr>

    <tr>
        <td colspan="2" class="terms">
          <label><?php echo $form['accept_tc']->render() ?>
            I have read and agree to the <a target="_blank" href="http://play.vodafone.co.nz/terms/nokia">Terms and Conditions</a></label>
          <?php echo $form['accept_tc']->renderError() ?>
        </td>
    </tr>

    <tr>
        <td colspan="2" height="50">
            <button type="submit"><img src="<?php echo application_asset_path('btn-submit.png') ?>" width="112" height="28" alt="Submit"> </button>            
                       
        </td>
    </tr>
    </table>
    <?php echo $form['_csrf_token']->render() ?>
</form>

<script>
    
    function checkfields() {
    
        $("#error").html("");
        var strText = jQuery.trim($('#vodafone_n9_answer').val());
        
        if (!$('#vodafone_n9_name').val()) {
            $("#error").html("Please enter your name").fadeIn("fast");
            return false;
        } else if (!$('#vodafone_n9_email').val()) {
            $("#error").html("Please enter your email address").fadeIn("fast");
            return false;
        } else if (!$('#vodafone_n9_phone').val()) {
            $("#error").html("Please enter your phone number").fadeIn("fast");
            return false;
        } else if (!$('#vodafone_n9_photo').val()) {
            $("#error").html("Please select an image").fadeIn("fast");
            return false;
        } else if (!$("#vodafone_n9_accept_tc").is(':checked')) {
            $("#error").html("You must agree to Vodafone Terms and Conditions").fadeIn("fast");
            return false;
        } else {
            return true;
        }
        
    }
    
</script>