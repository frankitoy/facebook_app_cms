<?php 
  use_application_stylesheet('screen');
 ?>
<div class="container">
  <div id="header">
      <img src="<?php echo application_asset_path('header.jpg') ?>" width="501" height="246" alt="Win a Nokia N9. It's beautifully simple">
      <?php /*
      <a href="/ajax/sharer/?s=18&amp;appid=2530096808&amp;p[]=244260142253182" rel="dialog" onclick>
        <img src="<?php echo application_asset_path('share.png') ?>" alt="Share" width="83" height="45" class="share">
      </a> */ ?>
  </div>
  <div id="content">
      <div class="copy">
          <div class="top">
              <h2>This sleek smartphone with its all-screen design is all about style and simplicity, and Vodafone brings it to you for as little as $599.</h2>
                <p>The Nokia N9 is a beautiful all-screen smartphone that's incredibly fast and simple to use. With one of the best multitasking experiences of any smartphone available today, everything is just a swipe away. <a href="http://www.vodafone.co.nz/shop/mobileDetails.jsp?skuId=sku13810112" target="_blank">Find out more</a></p>
              <p class="bold">Get great value with Vodafone. <a href="http://www.vodafone.co.nz/phones-plans/plans/smart/" target="_blank">Get it here</a></p>
          </div>
          <div class="left">
              <h2>You could win one of three beautifully simple Nokia N9s.</h2>
              <p>Simply upload a picture of something that portrays ‘beauty in design’ to you and you’ll go in the draw. </p>
          </div>
      </div>
    
        <div id="gallery_wrapper">
          <h2>Picture Gallery</h2>
          <?php include_component('vodafoneN9', 'gallery') ?>
        </div>        
  </div>
    
  <div id="form">
    <?php if(isset($submitted) and $submitted == true): ?>
      <div class="thanks">
        <h2>Thanks for entering. Good luck!</h2>
        <p><a href="/vodafone-nokia-n9">Enter again</a></p>
      </div>
    <?php else: ?>  
      <h2>Entry form</h2>
      <?php include_component('vodafoneN9', 'entryForm') ?>
    <?php endif; ?>
  </div>
    
</div>
<div id="footer">
  <p>&copy; 2011 Vodafone New Zealand Ltd. | <a href="http://www.vodafone.co.nz" target="_blank">vodafone.co.nz</a> | <a href="http://play.vodafone.co.nz/terms/nokia">Terms &amp; Conditions</a> | <a href="http://www.vodafone.co.nz/about/legal-stuff/vodafone-privacy-policy.jsp">Privacy Policy</a></p>
  <p>This promotion is in no way sponsored, endorsed or administered by, or associated with, Facebook. <br>
    By entering this competition, you agree to our terms &amp; conditions</p>
</div>