<form action="<?php echo application_url($page) ?>" method="post">

  <div class="form_row answer_form_row">
    <?php echo $form['answer'] ?>
  </div>
  
  <div class="form_row first_name_form_row">
    <?php echo $form['first_name'] ?>
  </div>
  
  <div class="form_row surname_form_row">
    <?php echo $form['surname'] ?>
  </div>
  
  <div class="form_row email_form_row">
    <?php echo $form['email'] ?>
  </div>
  
  <div class="form_row mobile_phone_form_row">
    <?php echo $form['mobile_phone'] ?>
  </div>
  
  <div class="form_row state_form_row">
    <?php echo $form['state'] ?>
  </div>
  
  <div class="form_row terms_form_row">
    <?php echo $form['terms'] ?>
    <a href="<?php echo application_landing_url('terms') ?>" class="terms_label fbalert" title="Terms &amp; Conditions" target="_blank">I have read and understood the Terms and Conditions and am over the age of 18*</a>
  </div>
  
  <input id="submit_button" type="image" src="<?php echo application_asset_path('btn-submit-entry.png') ?>"/>
  
  <?php echo $form->renderHiddenFields() ?>
  
</form>

<a id="privacy_link" class="fbalert" title="Privacy Policy" target="_blank" href="<?php echo application_landing_url('privacy') ?>">Privacy Policy</a>

<script type="text/javascript">

<?php

$messages = array();

if ($form->getGlobalErrors())
{
  $messages[] = $form->getGlobalErrors();
}

foreach ($form as $field)
{
  if ($field->hasError())
  {
    $messages[] = (string) $field->getError();
  }
}

?>
  
var messages = <?php echo json_encode($messages) ?>;

</script>
