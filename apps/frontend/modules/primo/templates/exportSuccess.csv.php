<?php

$entries = $entries->getRawValue();

$filename = tempnam(sys_get_temp_dir(), '');

$fp = fopen($filename, 'w');

fputcsv($fp, array(
  'First Name',
  'Surname',
  'Email',
  'Mobile Phone',
  'State',
  'Answer',
  'Entry Date'
));

foreach ($entries as $entry)
{
  fputcsv($fp, array(
    $entry['first_name'],
    $entry['surname'],
    $entry['email'],
    $entry['mobile_phone'],
    $entry['state'],
    $entry['answer'],
    $entry['entry_date']
  ));
}

fclose($fp);

echo file_get_contents($filename);

unlink ($filename);
