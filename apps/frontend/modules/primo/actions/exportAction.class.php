<?php

class exportAction extends sfAction
{
  private
    $username = 'fuelplus',
    $password = 'phEdU7hE';
  
  public function preExecute()
  {
    header('WWW-Authenticate: Basic realm="Primo Entries"');
    
    if (empty($_SERVER['PHP_AUTH_USER']) || empty($_SERVER['PHP_AUTH_PW']) ||
       ($this->username != $_SERVER['PHP_AUTH_USER'] || $this->password != $_SERVER['PHP_AUTH_PW']))
    {
      header('HTTP/1.0 401 Unauthorized');
      echo 'Login required';
      exit;
    }
  }
  
  public function execute($request)
  {
    sfConfig::set('sf_web_debug', false);

    $this->getResponse()->setContentType('text/csv');
    $this->getResponse()->setHttpHeader('Content-disposition', 'attachment;filename=primo-entries.xls');

    $this->entries = Doctrine::getTable('PrimoEntry')->getEntriesForExport();
  }
}