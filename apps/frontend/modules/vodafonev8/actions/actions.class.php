<?php

/**
 * vodafonev8 actions.
 *
 * @package    Facebook
 * @subpackage vodafonev8
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class vodafonev8Actions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeSave(sfWebRequest $request)
  {
    
    $v8 = new Vodafonev8;
    
    $v8->name = $request->getParameter("name");
    $v8->mobile = $request->getParameter("mobile");
    $v8->email = $request->getParameter("email");
    $v8->score = $request->getParameter("score");
    
    $v8->save();
    
    exit;
  }
}