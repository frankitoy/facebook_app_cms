<?php include_partial('vodafonev8/header', array("menu_active" => "enter_quiz")) ?>

<div class="column_background">
    <div class="home_text">
        
        <p>TeamVodafone is coming to NZ so we’re giving you the chance to join Craig Lowndes and Jamie Whincup on the ultimate roadie from Auckland to Hamilton. Did we mention you’ll be in the back seat of a Holden driven by the boys?</p>

        <p>Enter now by taking the TeamVodafone roadie quiz. Get four out of five multi-choice questions correct and you’ll be in the draw. <a target="_blank" href="/vodafone-v8/terms-and-conditions">Terms and conditions</a> apply. </p>

        <p>Hint: all answers can be found throughout the articles on this Facebook page.</p>

        <p>So get your V8 kit ready, wave a Holden flag and start your engines.</p>


        <a href="/vodafone-v8/play-now"><img src="<?php echo application_asset_path('playnow_button.png') ?>" /></a>

        <div class="drivers">
            <img src="<?php echo application_asset_path('drivers.png') ?>" />
        </div>
    </div>
</div>
    
<?php include_partial('vodafonev8/footer') ?>