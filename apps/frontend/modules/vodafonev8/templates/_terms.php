<div class="center" style="margin-top: 10px; margin-left: 10px;">
    <div class="car_spec_bg">
        <div style="width: 448px; padding-right: 15px; line-height: 18px; height: 555px; overflow-y: scroll">
            <p><span class="red large">TeamVodafone Roadie Facebook Promotion Terms & Conditions</span></p><br />
            
            <p>Information published by Vodafone New Zealand Limited ("Promoter") on how to enter and prizes form part of these terms and conditions. Any entry not complying with these terms and conditions is invalid.

            <p>1. Offer is available to Vodafone customers only, who are at least 16 years old at the time of entry into this competition. Employees of Vodafone, its associated companies, and agencies associated with this promotion and their immediate families are ineligible to enter.</p>

            <p>2. You can enter the promotion by completing the TeamVodafone quiz on Facebook and filling in the entry form online. Each completed entry will be in the draw to win. You can enter the promotion as many times as you like.</p>
            
            <p>3. The Promotional Period ("Promotion Period") commences 8:00am Monday 2 April 2012 and ends 5:30pm Sunday 15 April 2012. </p>

            <p>4. Each entry will go into the draw to win either a prize or the grand prize, although one person may not win both.   </p>
            
            <p>5. The grand prize is a TeamVodafone Roadie experience (road trip) from Auckland to Hamilton with TeamVodafone drivers, Craig Lowndes and Jamie Whincup on Thursday 19 April 2012 between 7am – 1pm. Any expenses incurred in relation to attending the TeamVodafone Roadie are the responsibility of the grand prize winner. These include but are not limited to:  accommodation, transport, food and beverage expenses. By entering this competition you are confirming your availability on Thursday 19 April between 7am – 1pm. </p>
            
            <p>6. If for any reason Craig Lowndes and Jamie Whincup are unavailable on the day due to a change in circumstances, then a prize package of $500 of TeamVodafone merchandise will be provided to the grand prize winner in lieu of the TeamVodafone Roadie experience.</p>
            
            <p>7. The prize will be one of ten prize packs which include ten items of TeamVodafone official merchandise.</p>
            
            <p>8. The grand prize and prize winners will be drawn at random at 5:00pm on Sunday 15 April 2012.</p>

            <p>9. All winners will be contacted within 24 hours. All winners will be notified by phone. If three reasonable attempts to contact the winner have been made within 24 hours from the draw and the winner is still unavailable, the promoter will choose another valid entry as the winner. </p>

            <p>10. The winners will be drawn at random from all valid entries. Promoter’s decision is final and no correspondence will be entered into.</p>

            <p>11. The Promoter reserves the right to verify the validity of entries and reserves the right to disqualify any entrant for tampering with the entry process, including but not limited to submitting an entry that is not in accordance with these terms and conditions.</p>

            <p>12. Prizes are not transferable and cannot be taken as cash, unless otherwise stipulated.</p>

            <p>13. The Promoter is not liable for any loss or damage whatsoever which is suffered, including but not limited to indirect or consequential loss, or for personal injury suffered or sustained during the course of accepting or using the prize, except for any liability which cannot be excluded by law.</p>

            <p>14. Winner consents to Promoter using their likeness, image and/or voice in the event they are a winner (including photograph, film and/or recording of the same) in any media for an unlimited period of time without remuneration for the purpose of promoting this competition (including any outcome), and promoting any products manufactured, distributed and/or supplied by Promoter.</p>

            <p>15. By entering the competition you are deemed to have accepted these terms and conditions.</p>

            <p>16. You can contact the promoters of the promotion by sending an email to sponsorship@vodafone.com. </p>

            <p>17. Vodafone reserves the right to update the Terms and Conditions and/or extend, cancel or amend this promotion at any time.</p>

            <p>This promotion is in no way sponsored, endorsed or administered by, or associated with, Facebook. You are providing your information to Vodafone NZ and not to Facebook. The information you provide will only be used for the purposes of this competition. By entering this competition, each entrant releases Facebook from any action or claim arising out of the competition. Any questions, comments or complaints regarding this competition must be directed to the Promoter.</p>

        </div>
    </div>
</div>
