<?php include_partial('vodafonev8/header', array("menu_active" => "car_spec")) ?>

<div class="center" style="margin-top: 10px; width: 500px">
    <img src="<?php echo application_asset_path('label_vodafonecarspecs.png')?>" />
    
    <div class="car_spec_bg">
        <div style="width: 450px;">
            <p><span class="red">Engine:</span> KRE developed 5.0 Litre V8 Holden 308 cast iron block and CNC aluminium heads. Category control MoTeC engine management control</p>
            
            <p><span class="red">Estimated power:</span> 635 +BHP limited to maximum 7,500 RPM</p>
            
            <p><span class="red">Gearbox:</span> 6-speed Holinger Sequential gearbox. Australian made with electronic gear cut</p>
            
            <p><span class="red">Differential:</span> Triple Eight Race Engineering designed rear axle assembly with Ford 9” spooled differential gears</p>
            
            <p><span class="red">Suspension:</span> Front – Triple Eight Race Engineered double wishbone  suspension with Sachs adjustable damper and cockpit adjustable front anti roll bar. Rear – Triple Eight 4 link rear suspension with adjustable watts link, coil over Sachs adjustable rear dampers with cockpit adjustable anti roll bar</p>
            
            <p><span class="red">Brakes:</span> Front – Control Alcon 6 piston front calipers with Alcon 375mm ventilated discs -Cockpit adjustable front/rear bias. Rear – Alcon 4 piston calipers with Alcon 343mm ventilated discs
                
            <p><span class="red">Wheels:</span> 17" x 11" aluminium racing rim
                
            <p><span class="red">Tyres:</span> Dunlop control tyre
                
            <p><span class="red">Fuel Capacity:</span> 80 litres maximum sprint tank and 120 litres for endurance races
                
            <p><span class="red">Vehicle weight:</span> 1355kg (category minimum without driver)
                
            <p><span class="red">Top Speed:</span> 298km/h
                
            <p><span class="red">Telemetry:</span> MoTeC and Freewave 900Mhz spread spectrum radio
                
            <p><span class="red">Data acquisition:</span> MoTeC ADL3 Display/Acquisition System. 256Mb memory, up to 100 channels of data at up to 5000 samples per second
                
            <p><span class="red">Seat:</span> Racetech 9119 Carbon with 6 point mounting system
                
            <p><span class="red">Steering wheel:</span> Triple Eight Race Engineering designed
        </div>
    </div>
    
    <img style="margin-left: -50px;" src="<?php echo application_asset_path('cars_spec_bottom.png')?>" />
</div>

<?php include_partial('vodafonev8/footer') ?>