<?php include_partial('vodafonev8/header', array("menu_active" => "profile_jamie")) ?>

<div class="center" style="margin-top: 10px; width: 500px">
    <img src="<?php echo application_asset_path('jamie_profile_image.png')?>" />
    
    <div class="car_spec_bg">
        <div style="width: 448px; padding-right: 15px; line-height: 18px; height: 555px; overflow-y: scroll">
            <p>
                <span class="red">Date of birth:</span> 6th February 1983<br />
                <span class="red">Lives:</span> Gold Coast, QLD, Australia<br />
                <span class="red">First Job:</span> Press Printer<br />
                <span class="red">First race car:</span> Van Dieman 94<br />
                <span class="red">Racing Hero:</span> Sir Jack Brabham<br />
                <span class="red">Favourite Race:</span> Clipsal 500<br />
                <span class="red">Greatest influence:</span> My Dad has always given me good advice
            </p>
            <p>Like most of Australia’s leading drivers, Whincup dominated in karting before graduating to Formula Ford in 2000. He was crowned Australian champion in 2002 and caught the eye of team owner Garry Rogers, who gave the Victorian his V8 Supercar debut at the Queensland 500 that year.</p>
            <p>The drive secured Whincup a spot at Garry Rogers Motorsport but after a tumultuous year he found himself without a driver come the end of the season. Tasman Motorsport offered Whincup a signing as a teammate for Jason Richards in 2005. Whincup didn’t disappoint, partnering Richards to podium results at Sandown and Bathurst events.</p>
            <p>The legendary pairing of Lowndes and Whincup was formed in 2006. Whincup went on to win his first race on debut, the Clipsal 500. The duo shone with a popular Bathurst victory.</p>
            <p>In 2007 Whincup proved a serious title contender, winning four rounds, on his way to finishing runner up to Garth Tander.</p>
            <p>2008 saw Whincup finish the job, taking his third Bathurst 1000 crown then moving on to claiming the championship. He became the only driver since Marcos Ambrose to achieve back-to-back championships in the highly competitive series.</p>
            <p>The Holden driver dominated the 2011 V8 Supercar Championship Series taking out the title - the second driver in history to do so in both a Holden and a Ford.</p>
            <p>With the Number One back on his TeamVodafone Holden Commodore, Jamie Whincup is the man to beat in the 2012 V8 Supercar Championship Series.</p>
        </div>
    </div>
</div>

<?php include_partial('vodafonev8/footer') ?>