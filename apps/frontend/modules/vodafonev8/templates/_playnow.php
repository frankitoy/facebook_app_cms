<?php include_partial('vodafonev8/header', array("menu_active" => "enter_quiz")) ?>

<div class="column_enterdetails center left" id="pre_entry">
    <div class="enternow_text">
        <p class="large red">ENTER YOUR DETAILS HERE</p><br />
        
        <p class="large">Name:<br /><input name="name" id="name" /></p>
        
        <p class="large">Mobile:<br /><input name="mobile" id="mobile" /></p>
        
        <p class="large">Email:<br /><input name="email" id="email" /></p><br />
        
        <p id="terms_label"><input type="checkbox" name="agree" id="agree" /> *I have agreed to the Promotion <a target="_blank" href="/vodafone-v8/terms-and-conditions">Terms & Conditions</a></p><br /><br />
        
        <img src="<?php echo application_asset_path('button_lets_play.png')?>" class="center button" id="show_question_1" />
    </div>
</div>



<div class="question_box center" style="line-height: 23px; display: none" id="question_1">
    <div class="flash_question">
        <img src="<?php echo application_asset_path('location_1.png')?>" id="q1_animation_location" style="display: none" />
        <img src="<?php echo application_asset_path('sign_1.png')?>" id="q1_animation_sign" class="sign" style="display: none" />
    </div>
    <div class="questions center large">
        <p>Q: Which year did Craig Lowndes and Jamie Whincup join forces as TeamVodafone?</p>
        <p id="answer_collection_1">
            <input type="radio" name="question_1" value="2007">2007<br />
            <input type="radio" name="question_1" value="2008">2008<br />
            <input type="radio" name="question_1" value="2011">2011
        </p>

        <p><img src="<?php echo application_asset_path('button_next_question.png')?>" class="right button" id="show_question_2" /></p>

        <p><img src="<?php echo application_asset_path('guage.png')?>" class="center" style="margin-top: 25px" /></p>
    </div>
</div>


<div class="question_box center" style="line-height: 23px; display: none" id="question_2">
    <div class="flash_question">
        <img src="<?php echo application_asset_path('location_2.png')?>" id="q2_animation_location" style="display: none" />
        <img src="<?php echo application_asset_path('sign_2.png')?>" id="q2_animation_sign" class="sign_2" style="display: none" />
    </div>
    <div class="questions center large">
        <p>Q: How many litres of fuel is required to fill the tank for a TeamVodafone endurance race?</p>
        <p id="answer_collection_2">
            <input type="radio" name="question_2" value="200">200<br />
            <input type="radio" name="question_2" value="120">120<br />
            <input type="radio" name="question_2" value="40">40
        </p>

        <p><img src="<?php echo application_asset_path('button_next_question.png')?>" class="right button" id="show_question_3" /></p>

        <p><img src="<?php echo application_asset_path('guage.png')?>" class="center" style="margin-top: 25px" /></p>
    </div>
</div>


<div class="question_box center" style="line-height: 23px; display: none" id="question_3">
    <div class="flash_question">
        <img src="<?php echo application_asset_path('location_3.png')?>" id="q3_animation_location" style="display: none" />
        <img src="<?php echo application_asset_path('sign_3.png')?>" id="q3_animation_sign" class="sign" style="display: none" />
    </div>
    <div class="questions center large">
        <p>Q: What was Jamie Whincup’s first job?</p>
        <p id="answer_collection_3">
            <input type="radio" name="question_3" value="Mechanic">Mechanic<br />
            <input type="radio" name="question_3" value="Press Printer">Press Printer<br />
            <input type="radio" name="question_3" value="Burger Marker">Burger Maker
        </p>

        <p><img src="<?php echo application_asset_path('button_next_question.png')?>" class="right button" id="show_question_4" /></p>

        <p><img src="<?php echo application_asset_path('guage.png')?>" class="center" style="margin-top: 25px" /></p>
    </div>
</div>


<div class="question_box center" style="line-height: 23px; display: none" id="question_4">
    <div class="flash_question">
        <img src="<?php echo application_asset_path('location_4.png')?>" id="q4_animation_location" style="display: none" />
        <img src="<?php echo application_asset_path('sign_4.png')?>" id="q4_animation_sign" class="sign_2" style="display: none" />
    </div>
    <div class="questions center large">
        <p>Q: Using the three images below, how many Vodafone logos can you see on the TeamVodafone V8 Supercar?</p>
        <p id="answer_collection_4">
            <input type="radio" name="question_4" value="2">2<br />
            <input type="radio" name="question_4" value="6">6<br />
            <input type="radio" name="question_4" value="1">1
        </p>
        <p>
            <img src="<?php echo application_asset_path('cars_question_4.png')?>" />
        </p>

        <p><img src="<?php echo application_asset_path('button_next_question.png')?>" class="right button" id="show_question_5" /></p>

        <p><img src="<?php echo application_asset_path('guage.png')?>" class="center" style="margin-top: 15px" /></p>
    </div>
</div>


<div class="question_box center" style="line-height: 23px; display: none" id="question_5">
    <div class="flash_question">
        <img src="<?php echo application_asset_path('location_5.png')?>" id="q5_animation_location" style="display: none" />
        <img src="<?php echo application_asset_path('sign_5.png')?>" id="q5_animation_sign" class="sign" style="display: none" />
    </div>
    <div class="questions center large">
        <p>Q: How many laps are in 'Race 6', the final on Sunday 21st April at the 2012 Hamilton 400?</p>
        <p id="answer_collection_5">
            <input type="radio" name="question_5" value="59">59<br />
            <input type="radio" name="question_5" value="30">30<br />
            <input type="radio" name="question_5" value="110">110
        </p>

        <p><img src="<?php echo application_asset_path('button_next_question.png')?>" class="right button" id="show_final_score" /></p>

        <p><img src="<?php echo application_asset_path('guage.png')?>" class="center" style="margin-top: 25px" /></p>
    </div>
</div>


<div class="win_box center text_center extra_large" style="line-height: 23px; display: none" id="final_score_win">
    <div class="score_background">
        <div class="final_text">
            <p>Game Over, you scored <span class="score"></span>/5 which means you qualify to go in the draw to be a back seat driver on the 'TeamVodafone Roadie'.</p><br />
            <img src="<?php echo application_asset_path('congrats.png')?>" />
        </div>
    </div>
    <div class="text_center">
        <img src="<?php echo application_asset_path('guage.png')?>" class="center" style="margin-top: 25px;" />
    </div>
</div>


<div class="win_box center" style="line-height: 23px; display: none" id="final_score_lose">
    <div class="score_background">
        <div class="final_text">
            <p>Game Over, unfortunately your score <span class="score"></span>/5 was not high enough to qualify for the 'TeamVodafone Roadie' prize. However you're in the draw to win a TeamVodafone prize pack. Good luck.</p>
            <a href="/vodafone-v8/team-vodafone"><img src="<?php echo application_asset_path('button_playagain.png')?>" /></a>
        </div>
    </div>
    <div class="text_center">
        <img src="<?php echo application_asset_path('guage.png')?>" class="center" style="margin-top: 25px;" />
    </div>
</div>

<?php include_partial('vodafonev8/footer') ?>