<div class="container">
    <div class="header"></div>
    <div class="twitter">
        <span class="twitter_content">
        <strong>Latest #TeamVodafone Tweets: </strong>
            <?php 
                $test = file_get_contents('https://search.twitter.com/search.json?q=from%3ATeamVodafone888');
                $test = json_decode($test, true);
                if (strlen($test['results'][0]['text']) > 100) {
                    echo substr($test['results'][0]['text'], 0, 100) . "...";
                } else {
                    echo $test['results'][0]['text'];
                }
            ?>
        </span>
    </div>
    
    <div class="column_left">
        <?php if($menu_active == 'enter_quiz'): ?>
            <a href="/vodafone-v8/team-vodafone"><img src="<?php echo application_asset_path('button_enterquiz.png') ?>" /></a><br />
        <?php else: ?>
            <a href="/vodafone-v8/team-vodafone"><img src="<?php echo application_asset_path('button_enterquiz_off.png') ?>" /></a><br />
        <?php endif; ?>
        
        <?php if($menu_active == 'profile_jamie'): ?>
            <a href="/vodafone-v8/profile-jamie"><img src="<?php echo application_asset_path('button_driverprofile_jamie.png') ?>" /></a><br />
        <?php else: ?>
            <a href="/vodafone-v8/profile-jamie"><img src="<?php echo application_asset_path('button_driverprofile_jamie_off.png') ?>" /></a><br />
        <?php endif; ?>
        
        <?php if($menu_active == 'profile_craig'): ?>
            <a href="/vodafone-v8/profile-craig"><img src="<?php echo application_asset_path('button_driverprofile_craig.png') ?>" /></a><br />
        <?php else: ?>
            <a href="/vodafone-v8/profile-craig"><img src="<?php echo application_asset_path('button_driverprofile_craig_off.png') ?>" /></a><br />
        <?php endif; ?>
        
        <?php if($menu_active == 'download_cover'): ?>
            <a href="/vodafone-v8/download-cover"><img src="<?php echo application_asset_path('button_download_cover.png') ?>" /></a><br />
        <?php else: ?>
            <a href="/vodafone-v8/download-cover"><img src="<?php echo application_asset_path('button_download_cover_off.png') ?>" /></a><br />
        <?php endif; ?>
            
        <?php if($menu_active == 'car_spec'): ?>
            <a href="/vodafone-v8/car-spec"><img src="<?php echo application_asset_path('button_carspecs.png') ?>" /></a><br />
        <?php else: ?>
            <a href="/vodafone-v8/car-spec"><img src="<?php echo application_asset_path('button_carspecs_off.png') ?>" /></a><br />
        <?php endif; ?>
    </div>
    
    <div class="column_right">
        <div class="countdown">
            <div class="timer_container"></div>
        </div>
        <a target="_blank" href="//www.facebook.com/sharer/sharer.php?u=http://facebook.satelliteapps.co.nz/13/share"><img src="<?php echo application_asset_path('facebook_share.png') ?>" /></a>
        <a target="_blank" href="http://twitter.com/#!/TeamVodafone888"><img src="<?php echo application_asset_path('twitter_share.png') ?>" style="margin-top: 10px" /></a><br />
    </div>
    
    <div class="column_center">