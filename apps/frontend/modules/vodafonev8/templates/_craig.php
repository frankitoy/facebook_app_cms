<?php include_partial('vodafonev8/header', array("menu_active" => "profile_craig")) ?>

<div class="center" style="margin-top: 10px; width: 500px">
    <img src="<?php echo application_asset_path('craig_profile_image.png')?>" />
    
    <div class="car_spec_bg">
        <div style="width: 448px; padding-right: 15px; line-height: 18px; height: 555px; overflow-y: scroll">
            <p>
                <span class="red">Date of birth:</span> 21st June 1974<br />
                <span class="red">Lives:</span> Kilcoy, QLD, Australia<br />
                <span class="red">First Job:</span> Mechanic<br />
                <span class="red">First race car:</span> F/Ford Van Dieman 85<br />
                <span class="red">Racing Hero:</span> Ayrton Senna<br />
                <span class="red">Favourite Race:</span> Clipsal 500<br />
                <span class="red">Greatest influence:</span> Peter Brock
            </p>
            <p>One of racing’s most popular sporting heroes, Craig Lowndes made an early debut in Australian motorsport, competing in his first go-karting event at just nine years of age. He quickly rose through the ranks, winning the national Formula Ford title in 1993 and the Formula Holden Silver Star class in 1994.</p>
            <p>That success put him on the radar of prominent V8 Supercar team, Holden Racing Team (HRT), who he joined in 1996. He took out his first-ever championship round and went on to become the youngest ever Australian Touring Car Champion.</p>
            <p>After an international venture in 1997, Lowndes returned to Australia and the V8 Supercar scene. After a handful of seasons with limited success, Lowndes joined Triple Eight Race Engineering in 2005 and finished second in the championship that year.</p>
            <p>In 2008, Lowndes recorded a strong season, helping TeamVodafone to their inaugural team’s title. Along with team-mate Jamie Whincup, Lowndes won a history-making, third-consecutive Bathurst 1000.</p>
            <p>Lowndes shone in 2010, with his most prized victory coming at Bathurst with former teammate Mark Skaife. The duo also took honours at Phillip Island.</p>
            <p>In 2011 Lowndes had a hard fought battle against teammate Jamie Whincup, narrowly missing out on the 2011 V8 Supercar Championship Series title. The V8 Veteran was also honoured with the prestigious Barry Sheene Medal for 2011.</p>
        </div>
    </div>
</div>

<?php include_partial('vodafonev8/footer') ?>