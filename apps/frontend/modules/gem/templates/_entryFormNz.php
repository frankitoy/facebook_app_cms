<form action="<?php echo application_url($page) ?>" method="post">

  <div class="form_row answer_form_row common_field_style">
    <?php echo $form['answer'] ?>
  </div>
  
  <div class="form_row first_name_form_row common_field_style">
    <?php echo $form['first_name'] ?>
  </div>
  
  <div class="form_row surname_form_row common_field_style">
    <?php echo $form['surname'] ?>
  </div>
  
  <div class="form_row email_form_row common_field_style">
    <?php echo $form['email'] ?>
  </div>
  
  <div class="form_row mobile_phone_form_row common_field_style">
    <?php echo $form['mobile_phone'] ?>
  </div>
  
  <div class="form_row town_form_row common_field_style">
    <?php echo $form['town'] ?>
  </div>
  
  <div class="form_row dob_form_row common_field_style">
    <div class="dob_container"><?php echo $form['dob'] ?></div>
  </div>
  
  <div class="form_row terms_form_row">
    <?php echo $form['terms'] ?>
    <a href="<?php echo application_landing_url('terms') ?>" class="terms_label facebook_popup" title="Terms &amp; Conditions" target="_blank">I have read and understood the Terms and Conditions and am over the age of 18*</a>
  </div>
  
  <input id="submit_button" type="image" src="<?php echo application_asset_path('button-enter-now.png') ?>"/>
  
  <?php echo $form->renderHiddenFields() ?>
  
</form>

<a id="privacy_link" class="facebook_popup" title="Privacy Policy" target="_blank" href="<?php echo application_landing_url('privacy') ?>">Privacy Policy</a>

<script type="text/javascript">

<?php

$messages = array();

if ($form->getGlobalErrors())
{
  $messages[] = $form->getGlobalErrors();
}

foreach ($form as $field)
{
  if ($field->hasError())
  {
    $messages[] = (string) $field->getError();
  }
}

?>
  
var messages = <?php echo json_encode($messages) ?>;

</script>
