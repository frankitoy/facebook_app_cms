<?php

class entryFormComponent extends sfComponent
{ 
  public function execute($request)
  {
    $this->application = $this->getRequest()->getAttribute('application');
    $this->page = $this->getRequest()->getAttribute('page');
    
    $this->form = new GemEntryForm;
    
    if ($request->isMethod('post'))
    {
      if ($this->form->bindAndSave($request->getParameter($this->form->getName())))
      {
        $this->getController()->redirect(array('sf_route' => 'page', 'app_slug' => $this->application->slug, 'page_slug' => 'thanks'));
      }
    }
  }
}