<?php

class MaiFmEventPixAdminPhotoForm extends MaiFmEventPixPhotoForm
{
	public function configure()
	{
		parent::configure();
		
		unset(
          $this['photo'],
          $this['location'],
          $this['event'],
          $this['email']
		);
	}
}
