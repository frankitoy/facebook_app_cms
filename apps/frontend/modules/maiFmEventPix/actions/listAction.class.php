<?php

class listAction extends sfAction
{
  public function execute($request)
  {
	$pager = new sfDoctrinePager('MaiFmEventPixPhoto', 28); // 4 rows of 7
	$pager->setPage($request->getParameter('page'));
	$pager->setQuery(Doctrine::getTable('MaiFmEventPixPhoto')->getQueryForPhotoList());
	$pager->init();
	  
	$this->pager = $pager;
  }
}