<?php

class searchAction extends sfAction
{
  public function execute($request)
  {
    $code = strtoupper(trim($request->getParameter('code')));
    
    $photo = Doctrine::getTable('MaiFmEventPixPhoto')->findOneBy('viewing_code', $code);
    $photos = array();
    
    if ($photo && $photo->exists())
    {
      $photos[] = $photo;
    }
    
    $this->photos = $photos;
  }
}