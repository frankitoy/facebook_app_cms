<?php

class submitAction extends sfAction
{
  public function execute($request)
  {
  	$form = new MaiFmEventPixPhotoForm(null, null, false);
  	
  	$form->bind(
	  	array(
	  	    'email'    => $request->getParameter('email'),
	        'caption'  => $request->getParameter('caption'),
	        'event'    => $request->getParameter('event'),
			'is_public' => true
	    ),
	    array(
	       'photo' => $request->getFiles('photo')
        )
	);
  	
  	if (!$form->isValid())
  	{
        $this->messages = $this->enumerateErrorsFromForm($form);
        
        return sfView::ERROR;
  	}
  		
  	$conn = Doctrine::getTable('MaiFmEventPixPhoto')->getConnection();
  	
  	$conn->beginTransaction();
  	
  	try
  	{
  		$photo = $form->save();
  		
  		if ($email = $form->getValue('email'))
  		{
  			$photo->sendToEmailAddress($email, $this->getMailer());
  		}
  		
  		$conn->commit();
  	}
  	catch (Exception $ex)
  	{
  		$conn->rollback();
  		
  		$this->messages = array('email' => $ex->getMessage());
  		
  		return sfView::ERROR;
  	}
  	
  	$this->photo = $photo;
  	
  	return sfView::SUCCESS;
  }
  
  protected function enumerateErrorsFromForm(sfForm $form)
  {
    $messages = array();

	foreach ($form->getErrorSchema()->getErrors() as $field => $error)
	{
	  $messages[$field] = $error->getMessage();
	}
	
	return $messages;
  }
  
}