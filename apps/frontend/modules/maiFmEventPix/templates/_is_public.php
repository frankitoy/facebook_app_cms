<?php if ($mai_fm_event_pix_photo->is_public): ?>
    <?php echo link_to(image_tag('/sfDoctrinePlugin/images/tick.png', array('alt' => 'Published')), 'mai_fm_event_pix_photo_backend_unpublish', array('id' => $mai_fm_event_pix_photo->id), array('post' => true)) ?>
<?php else: ?>
    <?php echo link_to(image_tag('/sfDoctrinePlugin/images/delete.png', array('alt' => 'Unpublished')), 'mai_fm_event_pix_photo_backend_publish', array('id' => $mai_fm_event_pix_photo->id), array('post' => true)) ?>
<?php endif; ?>