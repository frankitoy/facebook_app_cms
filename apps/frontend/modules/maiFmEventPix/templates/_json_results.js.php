<?php

$photoJson = array();

foreach ($photos as $photo)
{
     $photoJson[] = array(
         'thumbnail' => $photo->thumbnail_web_path,
         'photo' => $photo->web_path,
         'caption' => $photo->caption
     );
}

echo json_encode($photoJson);
