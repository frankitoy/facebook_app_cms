<?php

/**
 * riu_whatsnew actions.
 *
 * @package    Facebook
 * @subpackage riu_whatsnew
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class riu_whatsnewActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
      
    $riu = new Ripitupwhatsnew();
    
    $riu->name = $request->getParameter("name");
    $riu->email = $request->getParameter("email");
    if ($request->getParameter("join_letter") == "Yes") {
        $riu->join_letter = "Yes";
    } else {
        $riu->join_letter = "No";
    }
    
    $riu->save();
    
    echo "<img src='/uploads/app/what-s-new-on-rip-it-up/thanks_for_entering.jpg' />";
    die();
  }
}
