<?php

class shareActions extends sfActions
{
  public function executeRedirect(sfWebRequest $request)
  {
    sfConfig::set('sf_web_debug', false);
    
    $this->forward404Unless($share = Doctrine::getTable('Share')->find($request->getParameter('id')));
    
    $this->share = $share;
  }
}