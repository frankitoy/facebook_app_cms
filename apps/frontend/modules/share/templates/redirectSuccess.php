<!DOCTYPE html>
<html xmlns:og="http://ogp.me/ns#">
  <head>
    <title><?php echo $share->title ?></title>
    <meta property="og:title" content="<?php echo $share->title ?>"/>
    <meta property="og:type" content="<?php echo $share->type ?>"/>
    <meta property="og:url" content="<?php echo url_for(array('sf_route' => 'share', 'id' => $share->id), true) ?>"/>
    <meta property="og:image" content="<?php echo image_path($share->getImageWebPath(), true) ?>"/>
    <meta property="og:site_name" content="<?php echo $share->title ?>"/>
    <meta property="og:description" content="<?php echo $share->description ?>"/>
    <meta http-equiv="refresh" content="0;url=<?php echo $share->url ?>" />
    <script type="text/javascript">
      location.href = "<?php echo $share->url ?>";
    </script>
  </head>
  <body>
  </body>
</html>
