<?php

/**
 * vodafone50k actions.
 *
 * @package    Facebook
 * @subpackage vodafone50k
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class vodafone50kActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    die();
  }
  
  public function executeDownload(sfWebRequest $request) {
      if ($request->getParameter("password") == "vodafone50k!") {
            $all_registration = Doctrine::getTable("Vodafone50k")->findAll()->getData();
        
            // xls name
            $filename ="entries.xls";

            // heading contents
            $contents = "Name \t Phone \t Asnwer \n";


            foreach($all_registration as $registration) {

                $contents .= 
                    $registration->name . "\t " .
                    $registration->phone . "\t " .
                    $registration->answer . "\n ";
            }

            header('Content-type: application/ms-excel');
            header('Content-Disposition: attachment; filename='.$filename);

            echo $contents;

            die();
        } else {
            
        }
  }
  
  public function executeSave(sfWebRequest $request) 
  {
         $vodafone50k = new Vodafone50k();
         
         $vodafone50k->name = $request->getParameter("name");
         $vodafone50k->phone = $request->getParameter("phone");
         $vodafone50k->answer = $request->getParameter("answer");
         
         $vodafone50k->save();
  }
}
