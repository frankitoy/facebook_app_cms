<?php

/**
 * cas_gshock actions.
 *
 * @package    Facebook
 * @subpackage cas_gshock
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class cas_gshockActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
    
  public function executeDownload(sfWebRequest $request) {
      if ($request->getParameter("password") == "casio!") {
            $all_registration = Doctrine::getTable("XperiaArc")->findAll()->getData();
        
            // xls name
            $filename ="entries.xls";

            // heading contents
            $contents = "Name \t Email Address \t Phone \n";


            foreach($all_registration as $registration) {

                $contents .= 
                    $registration->first_name . "\t " .
                    $registration->email . "\t " .
                    $registration->answer . "\n ";
            }

            header('Content-type: application/ms-excel');
            header('Content-Disposition: attachment; filename='.$filename);

            echo $contents;

            die();
        } else {
            
        }
  }
  public function executeSave(sfWebRequest $request)
  {
    $casio = new XperiaArc();

    // let's recycle table xperia_arc(not in use) as our table
    $casio->first_name = $request->getParameter("name");
    $casio->email = $request->getParameter("email");
    $casio->answer = $request->getParameter("phone");
    
    // source
    $this->source = $request->getParameter("source_url");

    $casio->save();
  }
}
