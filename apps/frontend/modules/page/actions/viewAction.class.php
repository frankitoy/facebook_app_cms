<?php

class viewAction extends sfAction
{
  public function execute($request)
  {
    $this->application = Doctrine::getTable('Application')->findOneBySlug($request->getParameter('app_slug'));
    
    $this->getUser()->setApplication($this->application);
    
    $likePage = $this->application->LikePage;
    
    if ($likePage && $likePage->exists() && !$this->getUser()->likesCurrentPage())
    {
      $this->page = $likePage;
    }
    else
    {
      $this->page = $this->application->getPageBySlug($request->getParameter('page_slug'));
    }
    
    $this->forward404Unless($this->page && $this->page->exists());
    
    $this->getResponse()->setTitle($this->page->title);
    
    $this->getRequest()->setAttribute('application', $this->application);
    $this->getRequest()->setAttribute('page', $this->page);
  }
}