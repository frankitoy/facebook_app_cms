<?php

class defaultActions extends sfActions
{
  public function executeHomepage(sfWebRequest $request)
  {
    return $this->redirect('https://www.facebook.com/');
  }
  
  public function executeError404(sfWebRequest $request)
  {
    return $this->redirect('https://www.facebook.com/');
  }
}