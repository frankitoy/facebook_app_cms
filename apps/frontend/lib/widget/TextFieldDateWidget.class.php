<?php

class TextFieldDateWidget extends sfWidgetFormDate
{
  protected function renderDayWidget($name, $value, $options, $attributes)
  {
    unset(
      $options['choices']
    );
    
    $attributes['size'] = 2;
    
    $widget = new sfWidgetFormInputText($options, array_merge(array('maxlength' => 2), $attributes));
    return $widget->render($name, $value);
  }
  
  protected function renderMonthWidget($name, $value, $options, $attributes)
  {
    unset(
      $options['choices']
    );
    
    $attributes['size'] = 2;
    
    $widget = new sfWidgetFormInputText($options, array_merge(array('maxlength' => 2), $attributes));
    return $widget->render($name, $value);
  }
  
  protected function renderYearWidget($name, $value, $options, $attributes)
  {
    unset(
      $options['choices']
    );
    
    $attributes['size'] = 4;
    
    $widget = new sfWidgetFormInputText($options, array_merge(array('maxlength' => 4), $attributes));
    return $widget->render($name, $value);
  }
}