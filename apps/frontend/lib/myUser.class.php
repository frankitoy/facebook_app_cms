<?php

class myUser extends FacebookUser
{
  private $application;
  
  public function setApplication(Application $application)
  {
    $this->application = $application;
    
    $this->initFacebook();
  }
  
  public function getApplication()
  {
    return $this->application;
  }
  
  protected function initFacebook()
  {
    parent::init($this->application->app_id, $this->application->app_secret, $this->application->enable_cookie, null, null, $this->application->php_sdk_version);
  }
}
